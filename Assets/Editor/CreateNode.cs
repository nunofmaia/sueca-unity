using UnityEngine;
using UnityEditor;
using System;

public class CreateNode
{
    [MenuItem ("Assets/Create/Flowchart Action")]
    static void CreateAction()
    {
        Create("ActionTemplate.txt", "NewAction.cs");
    }

    [MenuItem ("Assets/Create/Flowchart Predicate")]
    static void CreatePredicate()
    {
        Create("PredicateTemplate.txt", "NewPredicate.cs");
    }
    
    [MenuItem ("Assets/Create/Flowchart Flow")]
    static void CreateFlow()
    {
        Create("FlowTemplate.txt", "NewFlow.cs");
    }

    static void Create(string template, string filename)
    {
        string currentPath = AssetDatabase.GetAssetPath(Selection.activeObject);
        string path = currentPath + "/" + filename;

        var DoCreateScriptAsset = Type.GetType("UnityEditor.ProjectWindowCallback.DoCreateScriptAsset, UnityEditor");
        ProjectWindowUtil.StartNameEditingIfProjectWindowExists(0,
                                                                ScriptableObject.CreateInstance(DoCreateScriptAsset) as UnityEditor.ProjectWindowCallback.EndNameEditAction,
                                                                path,
                                                                null,
                                                                "Assets/Editor/" + template);
    }
}
