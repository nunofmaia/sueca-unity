﻿using UnityEngine;
using UnityEditor;

//[CustomPropertyDrawer(typeof(Ranking))]
public class RankingDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        Rect rankRect = new Rect(position.x, position.y, 100, position.height);
        Rect valueRect = new Rect(position.x + 105, position.y, 50, position.height);


        EditorGUI.PropertyField(rankRect, property.FindPropertyRelative("rank"), GUIContent.none);
        EditorGUI.PropertyField(valueRect, property.FindPropertyRelative("value"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }

}
