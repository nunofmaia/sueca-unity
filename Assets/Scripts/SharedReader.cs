﻿using UnityEngine;

[RequireComponent(typeof(MarkerReaderUI))]
public class SharedReader : MonoBehaviour
{
    private MarkerReaderUI _reader;
    private bool hadMarkersBefore;
    private GameManager _manager;

    void Awake()
    {
        _reader = GetComponent<MarkerReaderUI>();
        _manager = GameManager.Instance;
    }

    void OnEnable()
    {
        hadMarkersBefore = false;
    }

    void Update()
    {
        if (GameManager.Instance.Type == GameManager.GameType.Simulation && Input.GetKeyDown(KeyCode.A))
        {
            EventManager.Trigger(Events.CARD_PLAYED, new IntEventArgs(0));
        }
        else
        {
            if (_reader.Markers.Count == 0 && hadMarkersBefore)
            {
                if (_manager.State.HasRenounced)
                {
                    EventManager.Trigger(Events.RENOUNCE, System.EventArgs.Empty);
                }
                else if (_manager.State.Tricks.Count == _manager.State.NumberOfTricks)
                {
                    EventManager.Trigger(Events.GAME_END, System.EventArgs.Empty);
                }
                else if (_manager.State.CurrentTrick.Plays.Count == _manager.State.PlaysPerTrick)
                {
                    EventManager.Trigger(Events.NEXT_TRICK, System.EventArgs.Empty);
                }
                hadMarkersBefore = false;
                return;
            }

            foreach (var marker in _reader.Markers)
            {
                EventManager.Trigger(Events.CARD_PLAYED, new IntEventArgs(marker.getSymbolID()));
                hadMarkersBefore = true;
            }

        }

    }
}
