﻿using UnityEngine;

public class BoxColliderUI : MonoBehaviour
{
    [HideInInspector]
    public BoxCollider2D BoxCollider = null;

    void Awake()
    {
        BoxCollider = gameObject.AddComponent<BoxCollider2D>();
    }

    public bool OverlapPoint(Vector2 point)
    {
        return BoxCollider.OverlapPoint(point);
    }

}
