﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class TrumpChooser : MonoBehaviour
{
    public Shuffle.Suit Suit;

    void Start()
    {
        GetComponent<Button>().onClick.AddListener(OnClick);
    }

    void OnClick()
    {
        GameManager.Instance.SetTrump(Suit);
    }
}
