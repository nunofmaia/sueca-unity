﻿using System.Collections;
using UnityEngine;

public class EnableAfter : MonoBehaviour
{
	public int Seconds;
	public GameObject Component;
	
	void OnEnable()
	{
		Component.SetActive(false);
		
		StartCoroutine(After(Seconds));
	}
	
	void OnDisable()
	{
		Component.SetActive(false);
	}
	
	IEnumerator After(int seconds)
	{
		yield return new WaitForSeconds(seconds);
		Component.SetActive(true);
	}
}
