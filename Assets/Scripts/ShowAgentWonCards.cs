﻿using UnityEngine;
using Shuffle;
using System.Collections.Generic;

public class ShowAgentWonCards : MonoBehaviour
{
    private AgentCard[] _cards;
    private IPlayer _agent;
    private Dictionary<char, Color> _suits;

    public enum TeamName
    {
        A,
        B
    }
    public TeamName Team;

    void Awake()
    {
        _cards = GetComponentsInChildren<AgentCard>();
        _suits = new Dictionary<char, Color>() { { '\u2660', Color.white }, { '\u2665', Color.red }, { '\u2663', Color.white }, { '\u2666', Color.red } };
    }

    void OnEnable()
    {
        if (_cards != null)
        {
            foreach (var card in _cards)
            {
                card.gameObject.SetActive(false);
            }
        }
    }

    void OnDisable()
    {
        if (_cards != null)
        {
            foreach (var card in _cards)
            {
                card.gameObject.SetActive(false);
            }
        }
    }

    void Start()
    {
        if (GameManager.Instance.Type == GameManager.GameType.Simulation)
        {
            _agent = PlayerManager.Instance.Players.Find(p => p.Id == 3);
        }
        else
        {
            _agent = PlayerManager.Instance.Players.Find(p => !p.IsHuman);
            if (_agent == null)
            {
                Destroy(gameObject);
            }
        }
    }

    void Update()
    {
        if (_agent == null) return;

        var tricks = GameManager.Instance.State.Tricks;
        var cardIndex = 0;

        foreach (var trick in tricks)
        {
            var winner = (ITeamPlayer)trick.Winner;
            var winnerTeamName = winner.Team.Name;

            if (Team.ToString() != winnerTeamName) continue;

            foreach (var play in trick.Plays)
            {
                if (play.PlayerId != _agent.Id) continue;

                var card = (PlayingCard)play.Content;
                var color = _suits[card.ToString()[1]];
                _cards[cardIndex].Rank1.text = card.ToString()[0].ToString();
                _cards[cardIndex].Rank2.text = card.ToString()[0].ToString();
                _cards[cardIndex].Suit1.text = card.ToString()[1].ToString();
                _cards[cardIndex].Suit2.text = card.ToString()[1].ToString();

                _cards[cardIndex].Suit1.color = color;
                _cards[cardIndex].Suit2.color = color;

                _cards[cardIndex].gameObject.SetActive(true);
                cardIndex++;
                break;
            }
        }
    }
}