﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class RoundChooser : MonoBehaviour
{
    public enum RoundSize
    {
        One = 1,
        Four = 4,
        Six = 6,
        Eight = 8
    }

    public RoundSize NumberOfRounds;

    void Start()
    {
        GetComponent<Button>().onClick.AddListener(OnClick);
    }

    void OnClick()
    {
        GameManager.Instance.SetRounds((int)NumberOfRounds);

        // Transition to LeadPlayerScreen while other behaviors are not implemented
        UIManager.SetActiveView("LeadPlayerScreen");
    }
}
