﻿using System.Collections.Generic;
using System.Linq;
using TUIO;
using UnityEngine;

namespace Uniducial
{
    public class TuioGroup
    {
        public float X { get; set; }

        public float Y { get; set; }

        public List<TuioObject> Elements { get; set; }

        public TuioGroup()
        {
            X = 0.0f;
            Y = 0.0f;
            Elements = new List<TuioObject>();
        }

        void Center()
        {
            int numberOfElements = Elements.Count;
            X = Elements.Aggregate(0.0f, (sum, el) => sum + el.getX()) / numberOfElements;
            Y = Elements.Aggregate(0.0f, (sum, el) => sum + el.getY()) / numberOfElements;
        }

        void Evaluate(int minimumNumberOfObjects)
        {
            var biggestGroup = Elements.GroupBy(elm => elm.getSymbolID())
                                       .OrderByDescending(k => k.Count())
                                       .First();

            if (biggestGroup.Count() > minimumNumberOfObjects)
            {
                Debug.Log(string.Format("This is a card with identifier {0}", biggestGroup.Key));
            }
        }

        public void Add(TuioObject obj)
        {
            Elements.Add(obj);
            Center();
            Debug.Log(this);
            Evaluate(1);

        }

        public double DistanceTo(float x, float y)
        {
            return 0;
        }

        public override string ToString()
        {
            return string.Format("[TuioGroup: X={0}, Y={1}, Elements={2}]", X, Y, Elements.Count);
        }
    }
}

