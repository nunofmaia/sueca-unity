﻿using Shuffle;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MarkerReaderUI))]
public class AgentTrumpCardReader : MonoBehaviour
{
    private MarkerReaderUI _reader;
    public int PlayerId;
    public int RobotNumber;

    void Awake()
    {
        _reader = GetComponent<MarkerReaderUI>();
        List<IPlayer> agentWithoutCards = PlayerManager.Instance.Players.FindAll(p => !p.IsHuman && (p.Hand == null || p.Hand.Count == 0));
        if (agentWithoutCards != null && agentWithoutCards.Count == 2 && RobotNumber <= 2)
        {
            PlayerId = agentWithoutCards[RobotNumber - 1].Id;
        }
        else if (agentWithoutCards != null && agentWithoutCards.Count == 1)
        {
            PlayerId = agentWithoutCards[0].Id;
        }
    }

    void Update()
    {
        if (_reader.isActiveAndEnabled)
        {
            foreach (var marker in _reader.Markers)
            {
                GameManager.ReceiveAgentTrumpCard(PlayerId, marker.getSymbolID());
            }
        }
    }

    void OnEnable()
    {
        if (PlayerId == PlayerManager.Instance.DealPlayerId)
        {
            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }
}
