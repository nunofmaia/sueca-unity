﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class UpdateTrump : MonoBehaviour
{
	private Text _component;

	void Awake()
	{
		_component = GetComponent<Text>();
	}

	void OnEnable()
	{
		var suit = GameManager.Instance.State.Trumps.ToString();
		var suits = new Dictionary<string, char>() { { "Spades", '\u2660'}, { "Hearts", '\u2665'}, { "Clubs", '\u2663'}, { "Diamonds", '\u2666'} };

        if (suits.ContainsKey(suit))
        {
		    _component.text = suits[suit].ToString();
		    if (suit == "Spades" || suit == "Clubs")
		    {
			    _component.color = Color.white;
		    }
		    else
		    {
			    _component.color = Color.red;
		    }
        }
	}
}
