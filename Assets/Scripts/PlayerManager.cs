﻿using System;
using System.Collections.Generic;
using System.Linq;
using Shuffle;
using SuecaTypes;
using UnityEngine;
using Rank = Shuffle.Rank;
using Suit = Shuffle.Suit;

public class QueuedAction
{
    public int PlayerId;
    public string Card;
    public string PlayInfo;

    public QueuedAction(int player, string card, string playInfo)
    {
        PlayerId = player;
        Card = card;
        PlayInfo = playInfo;
    }
}

public class PlayerManager : MonoBehaviour
{
    public IPlayer PlayerA;
    public IPlayer PlayerB;
    public IPlayer PlayerC;
    public IPlayer PlayerD;
    public List<IPlayer> Players;

    public static PlayerManager Instance { private set; get; }

    public ITeam TeamA;
    public ITeam TeamB;
    public int TeamAAccumulateScore;
    public int TeamBAccumulateScore;

    public bool TrumpReavealed;
    public int ActivePlayerId;
    private int _leadPlayerId;
    private List<QueuedAction> _queuedActions;

    private static ThalamusConnector _thalamusConnector;

    public static System.Random RandomGenerator = new System.Random(Guid.NewGuid().GetHashCode());
    public static int NumRobots = 0;
    public static int[] RobotsIds;

    void Awake()
    {
        Instance = this;
        TrumpReavealed = false;
        _queuedActions = new List<QueuedAction>();

        Players = new List<IPlayer>();
        TeamA = new Team(0, "A");
        TeamB = new Team(1, "B");

        _thalamusConnector = null;
    }

    void Update()
    {
        if (_queuedActions.Count <= 0)
        {
            return;
        }

        var qa = _queuedActions[0];
        _queuedActions.RemoveAt(0);

        //Play(qa.PlayerId, qa.Card);

        var queuedCard = JsonSerializable.DeserializeFromJson<Card>(qa.Card);

        var rank = (Rank)Enum.Parse(typeof (Rank), queuedCard.Rank.ToString());
        var suit = (Suit)Enum.Parse(typeof (Suit), queuedCard.Suit.ToString());

        var card = new PlayingCard(rank, suit);
        var playInfo = qa.PlayInfo;

        GameManager.ProcessCard(card, playInfo);
    }

    void OnDestroy()
    {
        Dispose();
    }

    public void Queue(QueuedAction qa)
    {
        _queuedActions.Add(qa);
    }

    public static void InitHumansWithAgent()
    {
        if (Instance.Players.Count > 0)
        {
            Instance.Reset();
            return;
        }

        _thalamusConnector = new ThalamusConnector(Instance);
        Instance.PlayerA = new TestSuecaPlayer(0, "P0", Instance.TeamA);
        Instance.PlayerB = new TestSuecaPlayer(1, "P1", Instance.TeamB);
        Instance.PlayerC = new TestSuecaPlayer(2, "P2", Instance.TeamA);
        Instance.PlayerD = new ThalamusPlayer(3, "EMYS", Instance.TeamB, _thalamusConnector);
        NumRobots = 1;
        RobotsIds = new int[] { 3 };

        Instance.Init();
    }

    public static void InitHumansWithTwoAgentsTeam()
    {
        if (Instance.Players.Count > 0)
        {
            Instance.Reset();
            return;
        }

        _thalamusConnector = new ThalamusConnector(Instance);
        Instance.PlayerA = new TestSuecaPlayer(0, "P0", Instance.TeamA);
        Instance.PlayerB = new ThalamusPlayer(1, "EMYS-1", Instance.TeamB, _thalamusConnector);
        Instance.PlayerC = new TestSuecaPlayer(2, "P2", Instance.TeamA);
        Instance.PlayerD = new ThalamusPlayer(3, "EMYS-2", Instance.TeamB, _thalamusConnector);
        NumRobots = 2;
        RobotsIds = new int[] { 1, 3 };

        Instance.Init();
    }

    public static void InitHumansWithTwoAgents()
    {
        Debug.Log("InitHumansWithTwoAgents");
        if (Instance.Players.Count > 0)
        {
            Instance.Reset();
            return;
        }

        Debug.Log("instantiations");
        _thalamusConnector = new ThalamusConnector(Instance);
        Instance.PlayerA = new TestSuecaPlayer(0, GameManager.Instance.player0Name, Instance.TeamA);
        Instance.PlayerB = new TestSuecaPlayer(1, GameManager.Instance.player1Name, Instance.TeamB);
        Instance.PlayerC = new ThalamusPlayer(2, "EMYS-1", Instance.TeamA, _thalamusConnector);
        Instance.PlayerD = new ThalamusPlayer(3, "EMYS-2", Instance.TeamB, _thalamusConnector);
        NumRobots = 2;
        RobotsIds = new int[] { 2, 3 };

        Instance.Init();
    }

    public static void InitWithHumans()
    {
        if (Instance.Players.Count > 0)
        {
            Instance.Reset();
            return;
        }

        Instance.PlayerA = new TestSuecaPlayer(0, "P0", Instance.TeamA);
        Instance.PlayerB = new HumanSuecaPlayer(1, "P1", Instance.TeamB);
        Instance.PlayerC = new TestSuecaPlayer(2, "P2", Instance.TeamA);
        Instance.PlayerD = new HumanSuecaPlayer(3, "P3", Instance.TeamB);
        NumRobots = 0;
        RobotsIds = new int[] { };

        Instance.Init();
    }

    private void Init()
    {
        TeamAAccumulateScore = 0;
        TeamBAccumulateScore = 0;

        TeamA.Players.Add(PlayerA);
        TeamA.Players.Add(PlayerC);
        TeamB.Players.Add(PlayerB);
        TeamB.Players.Add(PlayerD);

        Players.Add(PlayerA);
        Players.Add(PlayerB);
        Players.Add(PlayerC);
        Players.Add(PlayerD);
    }

    public static void SetLeadPlayer(int playerId)
    {
        Instance._leadPlayerId = playerId;
        Instance.ActivePlayerId = playerId;
    }

    public static IPlayer NextLeadPlayer()
    {
        Instance._leadPlayerId = (Instance._leadPlayerId + 1) % Instance.Players.Count;
        Instance.ActivePlayerId = Instance.LeadPlayerId;

        return Instance.Players.Find(p => p.Id == Instance._leadPlayerId);
    }

    public static IPlayer GetPlayer(int playerId)
    {
        return Instance.Players.Find(p => p.Id == playerId);
    }

    public static IPlayer GetTeamMate(int playerId)
    {
        var teamPlayerId = (playerId + 2) % Instance.Players.Count;
        return Instance.Players.Find(player => player.Id == teamPlayerId);
    }

    public static void AccumulateScores()
    {
        var teamAScore = Instance.TeamA.Score;

        if (teamAScore == 120)
        {
            Instance.TeamAAccumulateScore += 4;
        }
        else if (teamAScore > 90)
        {
            Instance.TeamAAccumulateScore += 2;
        }
        else if (teamAScore > 60)
        {
            Instance.TeamAAccumulateScore += 1;
        }
        else if (teamAScore == 0)
        {
            Instance.TeamBAccumulateScore += 4;
        }
        else if (teamAScore < 30)
        {
            Instance.TeamBAccumulateScore += 2;
        }
        else if (teamAScore < 60)
        {
            Instance.TeamBAccumulateScore += 1;
        }
    }

    private void ResetTeamScores()
    {
        TeamA.Score = 0;
        TeamB.Score = 0;
    }

    private void Reset()
    {
        TeamAAccumulateScore = 0;
        TeamBAccumulateScore = 0;

        ResetTeamScores();
    }

    public static int GetFloorId()
    {
        int index = RandomGenerator.Next(0, RobotsIds.Length);

        return RobotsIds.Length > 0 ? RobotsIds[index] : 0;
    }

    public static bool AddPlayerCard(int playerId, ICard card)
    {
        var player = Instance.Players[playerId];
        if (player.Hand == null) player.Hand = new List<ICard>();

        if (Instance.DealPlayerId == playerId && !Instance.TrumpReavealed && player.Hand.Count < 9)
        {
            player.Hand.Add(card);
            return true;
        }
        else if (Instance.DealPlayerId == playerId && Instance.TrumpReavealed && player.Hand.Count < 10)
        {
            player.Hand.Add(card);
            return true;
        }
        else if (Instance.DealPlayerId != playerId && player.Hand.Count < 10)
        {
            player.Hand.Add(card);
            return true;
        }

        return false;
    }

    public static bool AddPlayerTrumpCard(int playerId, ICard card)
    {
        var player = Instance.Players[playerId];
        if (player.Hand == null) player.Hand = new List<ICard>();

        if (Instance.DealPlayerId == playerId && !Instance.TrumpReavealed)
        {
            Instance.TrumpReavealed = true;
            player.Hand.Add(card);
            return true;
        }

        return false;
    }

    public static void ClearPlayerHands()
    {
        Instance.TrumpReavealed = false;
        foreach (var player in Instance.Players)
        {
            if (player.Hand != null)
            {
                player.Hand.Clear();
            }
        }
    }

    public static void Play(int player, string card, string playInfo)
    {
        Debug.Log(string.Format("Play {0}, {1}", player, Utils.DeserializeCard(card)));
        foreach (var p in Instance.Players)
        {
            p.Play(player, card, playInfo);
        }
        if (_thalamusConnector != null)
        {
            /*int floorId = player;
            if (!RobotsIds.Contains(player))
            {
                floorId = (player + 2) % 4;
            }*/
            _thalamusConnector.Play(player, card, playInfo, GetFloorId());
        }
    }

    public static void NextPlayer(int player)
    {
        Debug.Log(string.Format("NextPlayer {0}", player));
        foreach (var p in Instance.Players)
        {
            p.NextPlayer(player);
        }
        if (_thalamusConnector != null)
        {
            _thalamusConnector.NextPlayer(player, GetFloorId());
        }

        Instance.ActivePlayerId = player;
    }

    public static void GameEnd()
    {
        Debug.Log(string.Format("GameEnd {0}, {1}", Instance.TeamA.Score, Instance.TeamB.Score));
        foreach (var p in Instance.Players)
        {
            p.GameEnd(Instance.TeamA.Score, Instance.TeamB.Score);
        }
        if (_thalamusConnector != null)
        {
            _thalamusConnector.GameEnd(Instance.TeamA.Score, Instance.TeamB.Score, GetFloorId());
        }
    }

    public void Dispose()
    {
        foreach (var p in Instance.Players)
        {
            p.Dispose();
        }
        if (_thalamusConnector != null)
        {
            _thalamusConnector.Dispose();
        }
    }

    public static void GameStart(int gameId, ICard trumpCard)
    {
        int floorId = GetFloorId();
        Instance.ResetTeamScores();
        if (trumpCard == null)
        {
            var player = Instance.Players[Instance.DealPlayerId];
            trumpCard = player.Hand[0];
            GameManager.Instance.State.Trumps = (Suit) trumpCard.Symbol;
            Debug.Log("Trump card has been randomly set to " + trumpCard);
        }
        foreach (var p in Instance.Players.Cast<ITeamPlayer>())
        {
            Debug.Log(string.Format("GameStart {0}, {1}, {2}, {3}, {4}, {5}", gameId, p.Id, p.Team.Id, trumpCard, Instance.DealPlayerId, p.Hand.PrettyPrint()));
            p.GameStart(gameId, p.Id, p.Team.Id, trumpCard, Instance.DealPlayerId, p.Hand, Instance.Players[(p.Id + 2) % 4].Name, floorId);
        }
    }

    public static void SessionStart(int sessionId, int numGames, int[] agentsIds, int floorId)
    {
        Debug.Log(string.Format("SessionStart {0}, {1}", numGames, agentsIds));
        foreach (var player in Instance.Players)
        {
            player.SessionStart(sessionId, numGames, agentsIds);
        }
        if (_thalamusConnector != null)
        {
            _thalamusConnector.SessionStart(sessionId, numGames, agentsIds, floorId);
        }
    }

    public static void SessionEnd(int sessionId)
    {
        Debug.Log(string.Format("SessionEnd {0}, {1}", Instance.TeamAAccumulateScore, Instance.TeamBAccumulateScore));
        foreach (var player in Instance.Players)
        {
            player.SessionEnd(Instance.TeamAAccumulateScore, Instance.TeamBAccumulateScore);
        }
        if (_thalamusConnector != null)
        {
            _thalamusConnector.SessionEnd(sessionId, Instance.TeamAAccumulateScore, Instance.TeamBAccumulateScore, GetFloorId());
        }
    }

    public static void Shuffle()
    {
        var activePlayerId = PlayerManager.Instance.ActivePlayerId;
        var isHuman = PlayerManager.GetPlayer(activePlayerId).IsHuman;

        if (!isHuman)
        {
            activePlayerId = (activePlayerId + 3) % 4;
            isHuman = PlayerManager.GetPlayer(activePlayerId).IsHuman;
            if (!isHuman)
            {
                activePlayerId = (activePlayerId + 2) % 4;
                isHuman = PlayerManager.GetPlayer(activePlayerId).IsHuman;
                if (!isHuman)
                {
                    Debug.Log("RotateOnPlayer:OnEnable > Problem with robots");
                }
            }
        }

        Debug.Log(string.Format("Shuffle {0}", activePlayerId));
        foreach (var p in Instance.Players)
        {
            p.Shuffle(activePlayerId);
        }
        if (_thalamusConnector != null)
        {
            _thalamusConnector.Shuffle(activePlayerId, GetFloorId());
        }

        //Instance.ActivePlayerId = Instance.LeadPlayerId;
    }

    public static void Deal()
    {
        var activePlayerId = PlayerManager.Instance.DealPlayerId;
        var isHuman = PlayerManager.GetPlayer(activePlayerId).IsHuman;

        if (!isHuman)
        {
            activePlayerId = (activePlayerId + 3) % 4;
            isHuman = PlayerManager.GetPlayer(activePlayerId).IsHuman;
            if (!isHuman)
            {
                activePlayerId = (activePlayerId + 2) % 4;
                isHuman = PlayerManager.GetPlayer(activePlayerId).IsHuman;
                if (!isHuman)
                {
                    Debug.Log("RotateOnPlayer:OnEnable > Problem with robots");
                }
            }
        }

        Debug.Log(string.Format("Deal {0}", activePlayerId));
        foreach (var p in Instance.Players)
        {
            p.Deal(Instance.DealPlayerId);
        }
        if (_thalamusConnector != null)
        {
            _thalamusConnector.Deal(activePlayerId, GetFloorId());
        }

        Instance.ActivePlayerId = Instance.DealPlayerId;
    }

    public static void Cut()
    {
        Debug.Log(string.Format("Cut {0}", Instance.CutPlayerId));
        foreach (var p in Instance.Players)
        {
            p.Cut(Instance.CutPlayerId);
        }
        if (_thalamusConnector != null)
        {
            _thalamusConnector.Cut(Instance.CutPlayerId, GetFloorId());
        }

        Instance.ActivePlayerId = Instance.CutPlayerId;
    }

    public static void TrumpCard(ICard trumpCard)
    {
        Debug.Log(string.Format("Trump card {0} of player {1}", trumpCard.ToString(), Instance.DealPlayerId));
        foreach (var p in Instance.Players)
        {
            p.TrumpCard(trumpCard, Instance.DealPlayerId);
        }
        if (_thalamusConnector != null)
        {
            var rank = (SuecaTypes.Rank)Enum.Parse(typeof(SuecaTypes.Rank), trumpCard.Type.ToString());
            var suit = (SuecaTypes.Suit)Enum.Parse(typeof(SuecaTypes.Suit), trumpCard.Symbol.ToString());

            var card = new SuecaTypes.Card(rank, suit);
            var serializedCard = card.SerializeToJson();
            _thalamusConnector.TrumpCard(serializedCard, Instance.DealPlayerId, GetFloorId());
        }

        //Instance.ActivePlayerId = Instance.CutPlayerId;
    }

    public static void ReceiveRobotCards(int playerId)
    {
        Debug.Log("ReceiveRobotCards");
        foreach (var p in Instance.Players)
        {
            p.ReceiveRobotCards(playerId);
        }
        if (_thalamusConnector != null)
        {
            _thalamusConnector.ReceiveRobotCards(playerId, GetFloorId());
        }
    }

    public static void TrickEnd(int winnerId, int trickPoints)
    {
        Debug.Log(string.Format("TrickEnd {0}, {1}", winnerId, trickPoints));
        foreach (var p in Instance.Players)
        {
            p.TrickEnd(winnerId, trickPoints);
        }
        if (_thalamusConnector != null)
        {
            _thalamusConnector.TrickEnd(winnerId, trickPoints, GetFloorId());
        }
    }

    public static void Renounce(int playerId)
    {
        Debug.Log(string.Format("Renounce {0}", playerId));
        foreach (var p in Instance.Players)
        {
            p.Renounce(playerId);
        }
        if (_thalamusConnector != null)
        {
            _thalamusConnector.Renounce(playerId, GetFloorId());
        }
    }

    public static void GlanceAtScreen(double x, double y)
    {
        foreach (var p in Instance.Players)
        {
            p.GlanceAtScreen(x, y);
        }
        if (_thalamusConnector != null)
        {
            _thalamusConnector.GlanceAtScreen(x, y);
        }
    }

    public static void GazeAtScreen(double x, double y)
    {
        foreach (var p in Instance.Players)
        {
            p.GazeAtScreen(x, y);
        }
        if (_thalamusConnector != null)
        {
            _thalamusConnector.GazeAtScreen(x, y);
        }
    }

    public static void TargetScreenInfo(string targetName, int x, int y)
    {
        foreach (var p in Instance.Players)
        {
            p.TargetScreenInfo(targetName, x, y);
        }
        if (_thalamusConnector != null)
        {
            _thalamusConnector.TargetScreenInfo(targetName, x, y);
        }
    }

    // Traditional Sueca Way
    //public int LeadPlayerId { get { return Instance._leadPlayerId; }}
    //public int DealPlayerId { get { return (Instance._leadPlayerId + 3) % Instance.Players.Count; }}
    //public int CutPlayerId { get { return (Instance._leadPlayerId + 2) % Instance.Players.Count; } }

    // 2 Robots Way
    public int LeadPlayerId { get { return Instance._leadPlayerId; } }
    public int DealPlayerId { get { return (Instance._leadPlayerId + 2) % Instance.Players.Count; } }
    public int CutPlayerId { get { return (Instance._leadPlayerId + 2) % Instance.Players.Count; } }
}
