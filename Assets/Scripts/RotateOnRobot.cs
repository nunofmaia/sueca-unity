﻿using System.Collections.Generic;
using UnityEngine;

public class RotateOnRobot : MonoBehaviour
{

	void Awake()
	{
	}	
	void OnEnable()
	{
        var position = gameObject.transform.position;
        gameObject.transform.position = new Vector3(0, 0, 0);
        if (GameManager.Instance.Type == GameManager.GameType.TwoAgentsOpponents)
        {
            gameObject.transform.Rotate(Vector3.forward, 90);
            
        }
        gameObject.transform.position = position;
	}
	
	void OnDisable()
	{
        if (GameManager.Instance.Type == GameManager.GameType.TwoAgentsOpponents)
        {
            gameObject.transform.Rotate(Vector3.forward, -90);
        }
	}
}
