﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TrumpCardAnalyser : MonoBehaviour
{
    public Button ContinueButton;
    private bool card;
    private bool cardRemoved;

    void Start()
    {
        card = false;
        ContinueButton.interactable = false;
        EventManager.Listen(Events.TRUMP_CARD_READ, UpdateTrump);
        EventManager.Listen(Events.NO_CARDS, CardRemoved);
    }

    void UpdateTrump(EventArgs eventArgs)
    {
        card = true;
    }

    void CardRemoved(EventArgs eventArgs)
    {
        cardRemoved = true;

        if (card && cardRemoved)
        {
            ContinueButton.interactable = true;
        }
    }

    void OnEnable()
    {
        ContinueButton.interactable = false;
        card = false;
        cardRemoved = false;

    }
}