﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class LeadPlayerChooser : MonoBehaviour
{
    public enum PlayerPosition
    {
        Bottom,
        Right,
        Top,
        Left
    }

    public PlayerPosition Position;

    void Start()
    {
        GetComponent<Button>().onClick.AddListener(OnClick);
    }

    void OnClick()
    {
        GameManager.Instance.SetLeadPlayer((int)Position);
    }
}
