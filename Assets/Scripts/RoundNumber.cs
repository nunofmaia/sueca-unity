﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class RoundNumber : MonoBehaviour
{
	private Text _component;
	private GameManager _manager;
    void Start()
    {
		_component = GetComponent<Text>();
		_manager = GameManager.Instance;
    }

    void Update()
    {
        var round = _manager.State.Tricks.Count + 1;
        if (round > 10)
        {
            round = 10;
        }

		_component.text = string.Format("Vaza {0}", round);
    }
}
