﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class MoveIndicator : MonoBehaviour
{
    private RectTransform _component;
    public int PlayerId;

    void Awake()
    {
        _component = GetComponent<RectTransform>();
        _component.gameObject.SetActive(false);

        EventManager.Listen(Events.NEXT_PLAYER, Move);
        EventManager.Listen(Events.TRICK_END, Hide);
    }

    private void Move(EventArgs eventArgs)
    {
        _component.gameObject.SetActive(false);
        var activePosition = GameManager.Instance.State.ActivePlayer.Id;

        if (PlayerId == activePosition)
        {
            _component.gameObject.SetActive(true);
        }
    }

    private void Hide(EventArgs eventArgs)
    {
        _component.gameObject.SetActive(false);
    }
}
