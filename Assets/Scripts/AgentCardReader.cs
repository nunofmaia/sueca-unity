﻿using System;
using Shuffle;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MarkerReaderUI))]
public class AgentCardReader : MonoBehaviour
{
    private MarkerReaderUI _reader;
    private AgentTrumpCardReader _trumpCardReader;
    public int PlayerId;
    public int RobotNumber;

    void Awake()
    {
        _reader = GetComponent<MarkerReaderUI>();
        List<IPlayer> agentWithoutCards = PlayerManager.Instance.Players.FindAll(p => !p.IsHuman && (p.Hand == null || p.Hand.Count == 0));
        if (agentWithoutCards != null && agentWithoutCards.Count == 2 && RobotNumber <= 2)
        {
            PlayerId = agentWithoutCards[RobotNumber - 1].Id;
        }
        else if (agentWithoutCards != null && agentWithoutCards.Count == 1)
        {
            PlayerId = agentWithoutCards[0].Id;
        }
    }

    void Update()
    {
        if (_reader.Markers.Count == 0)
        {
            EventManager.Trigger(Events.NO_CARDS, EventArgs.Empty);
        }
        foreach (var marker in _reader.Markers)
        {
            GameManager.ReceivePlayerCard(PlayerId, marker.getSymbolID());
        }
    }

    void OnEnable()
    {
        List<AgentTrumpCardReader> temp = new List<AgentTrumpCardReader>(GetComponentsInChildren<AgentTrumpCardReader>(true));
        _trumpCardReader = temp.Find(r => r.PlayerId == PlayerId);
        if (_trumpCardReader == null)
        {
            Debug.Log("AGENTCARDREADER TCR is null");
        }
        if (PlayerId == PlayerManager.Instance.DealPlayerId)
        {
            if (_trumpCardReader != null)
            {
                _trumpCardReader.Show();
            }
        }
    }
}
