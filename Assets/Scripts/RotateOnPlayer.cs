﻿using System.Collections.Generic;
using UnityEngine;

public class RotateOnPlayer : MonoBehaviour
{
	private Dictionary<int, float> _angles;
	private float _lastAngle;
	private Vector3 _initialPosition;

	void Awake()
	{
		_angles = new Dictionary<int, float>() { { 0, 0 }, { 1, 90 }, { 2, 180 }, { 3, -90 } };
	}	
	void OnEnable()
	{
		var activePlayerId = PlayerManager.Instance.ActivePlayerId;
		var isHuman = PlayerManager.GetPlayer(activePlayerId).IsHuman;
		
		if (!isHuman)
		{
			activePlayerId = PlayerManager.GetTeamMate(activePlayerId).Id;
            isHuman = PlayerManager.GetPlayer(activePlayerId).IsHuman;
            if (!isHuman)
            {
                activePlayerId = (activePlayerId + 1) % 4;
                isHuman = PlayerManager.GetPlayer(activePlayerId).IsHuman;
                if (!isHuman)
                {
                    Debug.Log("RotateOnPlayer:OnEnable > Problem with robots");
                }
            }
		}
		
		var position = gameObject.transform.position;
		_initialPosition = position;
		
		var ratio = 0.15f;
		var screenHalfWidth = Screen.width/2;
		var screenHalfHeight = Screen.height/2;
		var widthOffset = screenHalfWidth * ratio;
		var heightOffset = screenHalfHeight * ratio;
		
		gameObject.transform.Rotate(Vector3.forward, _angles[activePlayerId]);
		_lastAngle = _angles[activePlayerId];
		
		switch (activePlayerId)
		{
			case 0:
				position = position + new Vector3(0, -screenHalfHeight + heightOffset, 0);
				break;
			case 1:
				position = position + new Vector3(Screen.width/2 - widthOffset, 0, 0);
				break;
			case 2:
				position = position + new Vector3(0, Screen.height/2 - heightOffset, 0);
				break;
			case 3:
				position = position + new Vector3(-Screen.width/2 + widthOffset, 0, 0);
				break;
		}
		
		gameObject.transform.position = position;
	}
	
	void OnDisable()
	{
		gameObject.transform.Rotate(Vector3.forward, -_lastAngle);
		gameObject.transform.position = _initialPosition;
	}
}
