﻿using Shuffle;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AgentCard : MonoBehaviour {

	public Text Rank1;
	public Text Rank2;
	public Text Suit1;
	public Text Suit2;
    public int PlayerId;

    public void OnClickCard()
    {
        if (PlayerManager.Instance.ActivePlayerId == PlayerId)
        {
            var ranks = new List<string>() { "2", "3", "4", "5", "6", "Q", "J", "K", "7", "A" };
            var suits = new List<char>() { '\u2663', '\u2666', '\u2665', '\u2660' };


            var rank = ranks.FindIndex(e => e == Rank1.text).ToString();
            var suit = suits.FindIndex(e => e == Suit1.text.ToCharArray()[0]).ToString();
            var card = rank + suit;

            Rank1.enabled = false;
            Rank2.enabled = false;
            Suit1.enabled = false;
            Suit2.enabled = false;
            GetComponentInParent<Image>().enabled = false;

            EventManager.Trigger(Events.SCREEN_CARD, new PlayEventArgs(card, -1));
        }
    }
}
