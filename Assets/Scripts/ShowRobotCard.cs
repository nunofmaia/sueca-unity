﻿using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class ShowRobotCard : MonoBehaviour
{
    private AgentCard _card;
    public int PlayerId;
    void Awake()
    {
        _card = GetComponentInChildren<AgentCard>();
        gameObject.SetActive(false);

        EventManager.Listen(Events.AGENT_CARD, OnCard);
        EventManager.Listen(Events.NEXT_TRICK, RemoveCard);
        //EventManager.Listen(Events.TRICK_END, RemoveCard);
        EventManager.Listen(Events.GAME_END, RemoveCard);
        EventManager.Listen(Events.RENOUNCE, RemoveCard);
        EventManager.Listen(Events.CLEAN_TABLE, RemoveCard);
    }

    private void RemoveCard(EventArgs eventArgs)
    {
        gameObject.SetActive(false);
    }

    private void OnCard(EventArgs eventArgs)
    {
        var args = (PlayEventArgs) eventArgs;
        var playerId = args.PlayerId;

        if (playerId == PlayerId)
        {
            var suits = new Dictionary<char, Color>() { { '\u2660', Color.white }, { '\u2665', Color.red }, { '\u2663', Color.white }, { '\u2666', Color.red } };
            var color = suits[args.Value[1]];

            _card.Rank1.text = args.Value[0].ToString();
            _card.Rank2.text = args.Value[0].ToString();
            _card.Suit1.text = args.Value[1].ToString();
            _card.Suit2.text = args.Value[1].ToString();

            _card.Suit1.color = color;
            _card.Suit2.color = color;

            gameObject.SetActive(true);
        }
    }
}
