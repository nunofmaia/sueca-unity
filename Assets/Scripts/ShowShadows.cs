﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MarkerReaderUI))]
public class ShowShadows : MonoBehaviour
{
    public GameObject Shadow;
	public int Quantity;
    private List<GameObject> _shadows;
    private MarkerReaderUI _reader;
	
    void Start()
    {
        _reader = GetComponent<MarkerReaderUI>();
        _shadows = new List<GameObject>();

        for (var i = 0; i < Quantity; i++)
        {
			var shadow = Instantiate(Shadow, Vector3.zero, Quaternion.identity) as GameObject;
			shadow.transform.SetParent(_reader.transform.parent);
			shadow.SetActive(false);
			_shadows.Add(shadow);
        }
    }
	
	void Update()
	{
		var index = 0;
        var uniqueMarkers = new List<int>();
		
		_shadows.ForEach(shadow => shadow.SetActive(false));
		foreach (var marker in _reader.Markers)
		{
		    var markerId = marker.getSymbolID();
			
			if (uniqueMarkers.Contains(markerId) || !IsCard(markerId)) continue;
            
            // We should check if it is an actual card being recognized...
            uniqueMarkers.Add(markerId);

			var shadow = _shadows[index];
			
			var xPos = _reader.InvertX ? 1 - marker.getX() : marker.getX();
			var yPos = _reader.InvertY ? 1 - marker.getY() : marker.getY();
			
			shadow.transform.position = new Vector3(xPos * Screen.width, (1 - yPos) * Screen.height, -10);
			shadow.SetActive(true);
			index++;

            //PlayerManager.TargetScreenInfo("cardPosition", (int)xPos * Screen.width, (int)yPos * Screen.height);
		}
	}

    bool IsCard(int markerId)
    {
        return GameManager.FullDeck.Exists(c => {
            var pc = (Shuffle.PlayingCard)c;
            return pc.BackId == markerId || pc.FrontId == markerId;
        });
    }
}
