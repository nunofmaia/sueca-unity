﻿using System;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class UpdateNumRounds : MonoBehaviour
{
    private Text _component;
    public Slider slider;

    void Awake()
    {
        _component = GetComponent<Text>();
    }

    void Update()
    {
        GameManager.Instance.SetRounds((int)slider.value);
        if (slider.value == 1)
        {
            _component.text = string.Format("{0} jogo por sessão", slider.value);
        }
        else
        {
            _component.text = string.Format("{0} jogos por sessão", slider.value);
        }
    }
}
