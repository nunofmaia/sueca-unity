﻿using System.Collections.Generic;
using UnityEngine.Events;

public class EventManager : Singleton<EventManager>
{
    private Dictionary<string, EventWithArgs> _events;

    void Awake()
    {
        _events = new Dictionary<string, EventWithArgs>();
    }

    public static void Listen(string eventName, UnityAction<System.EventArgs> listener)
    {
        EventWithArgs newEvent;
        if (Instance._events.TryGetValue(eventName, out newEvent))
        {
            newEvent.AddListener(listener);
        }
        else
        {
            newEvent = new EventWithArgs();
            newEvent.AddListener(listener);
            Instance._events.Add(eventName, newEvent);
        }
    }

    public static void Remove(string eventName, UnityAction<System.EventArgs> listener)
    {
        EventWithArgs existingEvent;
        if (Instance._events.TryGetValue(eventName, out existingEvent))
        {
            existingEvent.RemoveListener(listener);
        }
    }

    public static void Trigger(string eventName, System.EventArgs eventArgs)
    {
        EventWithArgs triggerEvent;
        if (Instance._events.TryGetValue(eventName, out triggerEvent))
        {
            triggerEvent.Invoke(eventArgs);
        }
    }
}

public class EventWithArgs : UnityEvent<System.EventArgs> { }

public class IntEventArgs : System.EventArgs
{
    public int Value;

    public IntEventArgs(int value)
    {
        Value = value;
    }
}

public class StringEventArgs : System.EventArgs
{
    public string Value;

    public StringEventArgs(string value)
    {
        Value = value;
    }
}

public class PlayEventArgs : System.EventArgs
{
    public string Value;
    public int PlayerId;

    public PlayEventArgs(string value, int playerId)
    {
        Value = value;
        PlayerId = playerId;
    }
}

public static class Events
{
    public static string NEXT_TRICK = "OnNextTrick";
    public static string NEXT_PLAYER = "OnNextPlayer";
    public static string AGENT_CARD = "OnAgentCard";
    public static string SCREEN_CARD = "OnScreenCard";
    public static string GAME_END = "OnGameEnd";
    public static string CARD_PLAYED = "OnCardPlayed";
    public static string AGENT_CARD_READ = "OnAgentCardRead";
    public static string TRUMP_CARD_READ = "OnTrumpCardRead";
    public static string TRICK_END = "OnTrickEnd";
    public static string RENOUNCE = "OnRenounce";
    public static string NO_CARDS = "OnNoCards";
    public static string CLEAN_TABLE = "OnCleanTable";
    

}

