﻿namespace Shuffle
{
    public interface ITeamPlayer : IPlayer
    {
        ITeam Team { get; }
    }
}
