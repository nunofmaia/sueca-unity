﻿using System.Collections.Generic;

namespace Shuffle
{
    public struct Trick
    {
        public List<Play> Plays;
        public IPlayer Winner;
        
        public Trick(List<Play> plays, IPlayer player)
        {
            Plays = plays;
            Winner = player;
        }
        
        public Trick(List<Play> plays) : this()
        {
            Plays = plays;
        }
    }
}