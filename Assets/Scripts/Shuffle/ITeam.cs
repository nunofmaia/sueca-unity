﻿using System.Collections.Generic;

namespace Shuffle
{
    public interface ITeam
    {
        int Id { get; }
        string Name { get; }
        List<IPlayer> Players { get; }
        int Score { set; get; }
    }
}
