﻿using System.Collections.Generic;

namespace Shuffle
{
    public interface ICardGame
    {
        List<ICard> Deck { get; }
        List<IPlayer> Players { get; }
        Play ActivePlay { get; }
        IPlayer ActivePlayer { set; get; }
    }
}
