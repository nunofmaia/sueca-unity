﻿namespace Shuffle
{
    public interface IFiducialObject
    {
        int FrontId { get; }
        int BackId { get; }
    }
}
