﻿using System.Collections.Generic;

namespace Shuffle
{
    public interface ITrickCardGame : ICardGame
    {
        List<Trick> Tricks { get; }
        Suit? Trumps { get; }
        Suit? LeadSuit { set; get; }
        int NumberOfTricks { get; }
        int PlaysPerTrick { get; }
        IPlayer TrickWinner { set; get; }
        Trick CurrentTrick { get; }
    }
}
