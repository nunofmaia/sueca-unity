﻿using System;
using System.Collections.Generic;

namespace Shuffle
{
    public interface ICard : IShuffleObject
    {
        Enum Type { get; }
        Enum Symbol { get; }
        int Value { get; }
        List<string> Rules { get; }
    }
}
