﻿using System;
using System.Collections.Generic;

namespace Shuffle
{
    public struct PlayingCard : ICard, IFiducialObject
    {
        public Rank Rank;
        public Suit Suit;

        public PlayingCard(Rank rank, Suit suit) : this()
        {
            Rank = rank;
            Suit = suit;
        }

        public Enum Type
        {
            get { return Rank; }
            set { Rank = (Rank)value; }
        }

        public Enum Symbol
        {
            get { return Suit; }
            set { Suit = (Suit)value; }
        }

        public int Value { set; get; }

        public List<string> Rules
        {
            get { return null; }
        }

        public override string ToString()
        {
            var ranks = new string[] {"2", "3", "4", "5", "6", "Q", "J", "K", "7", "A"};
            var suits = new Dictionary<string, char>() { { "Spades", '\u2660'}, { "Hearts", '\u2665'}, { "Clubs", '\u2663'}, { "Diamonds", '\u2666'} };
            return string.Format("{0}{1}", ranks[(int)Rank], suits[Suit.ToString()].ToString());
        }

        public int FrontId { set; get; }

        public int BackId { set; get; }
    }

    public enum Suit
    {
        Clubs,
        Diamonds,
        Hearts,
        Spades,
    }

    public enum Rank
    {
        Two, Three, Four, Five, Six,
        Queen,
        Jack,
        King,
        Seven,
        Ace
    }
}
