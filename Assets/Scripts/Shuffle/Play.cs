﻿namespace Shuffle
{
    public struct Play
    {
        public int PlayerId { set; get; }
        public IShuffleObject Content { set; get; }

        public Play(int playerId, IShuffleObject content) : this()
        {
            PlayerId = playerId;
            Content = content;
        }

        public override string ToString()
        {
            return string.Format("[Play, {0} {1}]", PlayerId, Content);
        }
    }
}
