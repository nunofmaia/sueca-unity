﻿using System.Collections.Generic;

namespace Shuffle
{
    public interface IPlayer : ISPerceptions
    {
        int Id { get; }
        string Name { set; get; }
        List<ICard> Hand { set; get; }
        int Score { set; get; }
        bool IsHuman { get; }
        bool CanChoosePlayOnScreen { get; }
    }
}
