﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class UpdateAccumulatedScore : MonoBehaviour {

    private PlayerManager _manager;
    private Text _component;

    void Start()
    {
        _component = GetComponent<Text>();
        _manager = PlayerManager.Instance;
        
        _component.text = string.Format("{0}  -  {1}", 0, 0);

    }
    
    void Update()
    {
        int currentAScore = _manager.TeamAAccumulateScore;
        int currentBScore = _manager.TeamBAccumulateScore;

        _component.text = string.Format("{0}  -  {1}", currentAScore, currentBScore);
    }
}
