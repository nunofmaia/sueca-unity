﻿#define DEBUG

using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(MarkerReaderUI))]
public class Statistics : MonoBehaviour
{
	public Text MarkerId;
	public Text Iterations;
	public Text Correct;
	public Text Incorrect;
	public Text Done;

	private int _id;
	private int _iterations;
	private int _currentIteration;
	private int _correct;
	private int _incorrect;
	private MarkerReaderUI _reader;

	void Start()
	{
		_id = int.Parse(MarkerId.text);
		_iterations = int.Parse(Iterations.text);
		_currentIteration = 0;
		_correct = 0;
		_incorrect = 0;

		_reader = GetComponent<MarkerReaderUI>();

		Correct.text = "0.0";
		Incorrect.text = "0.0";
		Done.enabled = false;

		Dbg.Trace("MarkerID: " + _id);
		Dbg.Trace("Iterations: " + _iterations);
	}

	void Update()
	{
		if (_currentIteration >= _iterations)
		{
			Done.enabled = true;

			Dbg.Trace(string.Format("Correct: {0}", _correct/(_iterations*1f)));
			Dbg.Trace(string.Format("Incorrect: {0}", _incorrect/(_iterations*1f)));
			Dbg.Trace("End trace");
			Destroy(this);
		}

		foreach (var marker in _reader.Markers)
		{
			if (marker.getSymbolID() == _id)
			{
				_correct++;
			}
			else
			{
				_incorrect++;
			}
		}

		if (_reader.Markers.Count > 0)
		{
			 _currentIteration++;
			 Correct.text = (_correct/(_currentIteration*1f)).ToString();
			Incorrect.text = (_incorrect/(_currentIteration*1f)).ToString();
		}
	}
}
