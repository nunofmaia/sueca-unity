﻿using System;
using System.Diagnostics;
using System.IO;
using UnityEngine;

public class Dbg : MonoBehaviour
{
	public string LogFile = "log.txt";
	public bool EchoToConsole = true;
	public bool AddTimeStamp = true;

	private StreamWriter OutputStream;
	private static Dbg _instance = null;

	public static Dbg Instance
	{
		get { return _instance; }
	}

	void Awake()
	{
		if (_instance != null)
		{
			UnityEngine.Debug.LogError("Multiple Dbg instances exist!");
			return;
		}

		_instance = this;
		#if !FINAL
		OutputStream = new StreamWriter(LogFile, true);
		#endif
	}

	void OnDestroy()
	{
		#if !FINAL
		if (OutputStream != null)
		{
			OutputStream.Close();
			OutputStream = null;
		}
		#endif
	}

	private void Write(string message)
	{
		#if !FINAL
		if (AddTimeStamp)
		{
			DateTime now = DateTime.Now;
			message = string.Format("[{0:H:mm:ss}] {1}", now, message);
		}

		if (OutputStream != null)
		{
			OutputStream.WriteLine(message);
			OutputStream.Flush();
		}

		if (EchoToConsole)
		{
			UnityEngine.Debug.Log(message);
		}
		#endif
	}

	//[Conditional("DEBUG"), Conditional("PROFILE")]
	public static void Trace(string message)
	{
		#if !FINAL
		if (Dbg.Instance != null)
		{
			Dbg.Instance.Write(message);
		}
		else
		{
			UnityEngine.Debug.Log(message);
		}
		#endif
	}
}
