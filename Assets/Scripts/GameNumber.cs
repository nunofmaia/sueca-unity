﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class GameNumber : MonoBehaviour
{
	private Text _component;
	private GameManager _manager;
    void Start()
    {
		_component = GetComponent<Text>();
		_manager = GameManager.Instance;
    }

    void Update()
    {
        var currentGame = _manager.CurrentGame + 1;
        var numberGames = _manager.NumberOfRounds;

        _component.text = string.Format("Jogo {0}/{1}", currentGame, numberGames);
    }
}
