﻿using System.Collections.Generic;
using Shuffle;
using System.Linq;

public class Team : Shuffle.ITeam
{

    public Team(int id, string name)
    {
        Id = id;
        Name = name;
        Players = new List<IPlayer>();
    }

    public int Id { get; private set; }
    public string Name { get; private set; }
    public List<IPlayer> Players { get; private set; }

    public int Score
    {
        set { Players.ForEach(p => p.Score = value); }
        get { return Players.Sum(p => p.Score); }
    }
}
