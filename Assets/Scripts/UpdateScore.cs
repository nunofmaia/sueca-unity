﻿using System;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class UpdateScore : MonoBehaviour
{
    private Text _component;
    private PlayerManager _manager;

    void Awake()
    {
        _component = GetComponent<Text>();
        _manager = PlayerManager.Instance;
    }

    void Update()
    {
        int currentAScore = _manager.TeamA.Score;
        int currentBScore = _manager.TeamB.Score;
        
        _component.text = string.Format("{0}           {1}", currentAScore, currentBScore);
    }
}
