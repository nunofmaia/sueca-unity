﻿using System.Collections.Generic;
using System.Linq;
using TUIO;
using UnityEngine;
using Uniducial;
using UnityEngine.UI;

public delegate void MarkerHandler(int marker);

[RequireComponent(typeof(BoxColliderUI))]
public class MarkerReaderUI : MonoBehaviour
{
    public bool InvertX;
    public bool InvertY;

    TuioManager _tuioManager;
    Camera _mainCamera;
    BoxColliderUI _boxCollider;

    void Awake()
    {
        _boxCollider = gameObject.GetComponent<BoxColliderUI>();
        _tuioManager = TuioManager.Instance;

        if (!_tuioManager.IsConnected) _tuioManager.Connect();

        Markers = new List<TuioObject>();
    }

    void OnDestroy()
    {
        if (_tuioManager.IsConnected)
        {
            _tuioManager.Disconnect();
            Debug.Log("TuioManager was disconnected");
        }
    }

    void Start()
    {
        _mainCamera = GameObject.FindGameObjectWithTag("MainCamera").camera;
        if (_mainCamera == null)
        {
            Debug.LogError("There is no main camera defined in your scene.");
        }
        
        var image = gameObject.GetComponent<Image>();
        RawImage newImage;
        Rect box;
        if (image == null)
        {
            newImage = gameObject.GetComponent<RawImage>();
            box = newImage.GetPixelAdjustedRect();
        }
        else
        {
            box = image.GetPixelAdjustedRect();
        }
        
        _boxCollider.BoxCollider.size = new Vector3(box.width, box.height, 0);
    }

    void Update()
    {
        Markers.Clear();
        foreach (var marker in _tuioManager.GetMarkers().Where(IsInsideCollider))
        {
            Markers.Add(marker);
        }
    }

    void OnApplicationQuit()
    {
        if (_tuioManager.IsConnected)
        {
            _tuioManager.Disconnect();
        }
    }

    bool IsInsideCollider(TuioObject marker)
    {
        var x = InvertX ? 1 - marker.getX() : marker.getX();
        var y = InvertY ? 1 - marker.getY() : marker.getY();

        var position = new Vector2(x * Screen.width, (1 - y) * Screen.height);

        return _boxCollider.OverlapPoint(position);
    }

    //void UseCollider(TUIO.TuioObject marker)
    //{
    //    var x = InvertX ? 1 - marker.X : marker.X;
    //    var y = InvertY ? 1 - marker.Y : marker.Y;

    //    var position = new Vector2(x * Screen.width, (1 - y) * Screen.height);

    //    if (!_boxCollider.OverlapPoint(position))
    //    {
    //        return;
    //    }

    //    Markers.Add(marker);
    //}

    public List<TuioObject> Markers { get; private set; }
}
