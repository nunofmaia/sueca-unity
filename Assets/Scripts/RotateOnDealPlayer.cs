﻿using System.Collections.Generic;
using UnityEngine;

public class RotateOnDealPlayer : MonoBehaviour
{
	private Dictionary<int, float> _angles;
	private float _lastAngle;
	private Vector3 _initialPosition;
    public bool TransformPosition;
    public bool EscapeRobots;

	void Awake()
	{
		_angles = new Dictionary<int, float>() { { 0, 0 }, { 1, 90 }, { 2, 180 }, { 3, -90 } };
	}	
	void OnEnable()
	{
		var activePlayerId = PlayerManager.Instance.DealPlayerId;

        if (EscapeRobots)
        {
            var isHuman = PlayerManager.GetPlayer(activePlayerId).IsHuman;

            if (!isHuman)
            {
                activePlayerId = PlayerManager.GetTeamMate(activePlayerId).Id;
                isHuman = PlayerManager.GetPlayer(activePlayerId).IsHuman;
                if (!isHuman)
                {
                    activePlayerId = (activePlayerId + 1) % 4;
                    isHuman = PlayerManager.GetPlayer(activePlayerId).IsHuman;
                    if (!isHuman)
                    {
                        Debug.Log("RotateOnPlayer:OnEnable > Problem with robots");
                    }
                }
            }
        }

		
		var position = gameObject.transform.position;
		_initialPosition = position;
		
		var ratio = 0.4f;
		var screenHalfWidth = Screen.width/2;
		var screenHalfHeight = Screen.height/2;
		var widthOffset = screenHalfWidth * ratio;
		var heightOffset = screenHalfHeight * ratio;
		
		gameObject.transform.Rotate(Vector3.forward, _angles[activePlayerId]);
		_lastAngle = _angles[activePlayerId];

        if (TransformPosition)
        {
		    switch (activePlayerId)
		    {
			    case 0:
				    position = position + new Vector3(0, -screenHalfHeight + heightOffset, 0);
				    break;
			    case 1:
                    position = position + new Vector3(screenHalfWidth - widthOffset, 0, 0);
				    break;
			    case 2:
                    position = position + new Vector3(0, screenHalfHeight - heightOffset, 0);
				    break;
			    case 3:
                    position = position + new Vector3(-screenHalfWidth + widthOffset, 0, 0);
				    break;
		    }
            gameObject.transform.position = position;
        }
	}
	
	void OnDisable()
	{
		gameObject.transform.Rotate(Vector3.forward, -_lastAngle);
        if (TransformPosition)
        {

		    gameObject.transform.position = _initialPosition;
        }
	}
}
