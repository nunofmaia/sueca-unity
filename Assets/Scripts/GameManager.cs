﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Flow;
using Newtonsoft.Json;
using Shuffle;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public enum GameType
    {
        Simulation,
        OnlyHumans,
        WithAgent,
        TwoAgentsOpponents,
        TwoAgentsTeam
    }
    public TextAsset ListOfCards;
    public Flowchart Rules;

    [HideInInspector]
    public GameType Type;

    public static GameManager Instance { private set; get; }
    public static List<ICard> FullDeck;

    public int NumberOfRounds;
    public int CurrentGame;
    public string player0Name = "P0";
    public string player1Name = "P1";
    private int SessionId;
    private int ShouldGreet;

    void Awake()
    {
        Instance = this;
        SessionId = 0;
        ShouldGreet = 1;
    }

    private void InitGameState()
    {
        var cards = Utils.LoadTextAsset<List<PlayingCard>>(ListOfCards);
        State = new Sueca();
        cards.Shuffle();
        cards.ForEach(card => State.Deck.Add(card));

        // Copy the original deck
        FullDeck = new List<ICard>();
        cards.ForEach(card => FullDeck.Add(card));

        PlayerManager.ClearPlayerHands();
        if (Type == GameType.Simulation)
        {
            var deck = new List<ICard>();
            State.Deck.ForEach(card => deck.Add(card));

            PlayerManager.Instance.Players.ForEach(player =>
            {
                var hand = Utils.Deal(deck, 10);
                player.Hand = hand;
            });
        }
    }

    void Start()
    {
        EventManager.Listen(Events.CARD_PLAYED, OnPlay);
        EventManager.Listen(Events.NEXT_TRICK, OnNextTrick);
        EventManager.Listen(Events.GAME_END, OnGameEnd);
        EventManager.Listen(Events.RENOUNCE, OnRenounce);
        EventManager.Listen(Events.SCREEN_CARD, onScreenPlay);

        UIManager.ResetViews();
        //UIManager.SetActiveView("StartScreen");
        UIManager.SetActiveView("InitScreen");
    }

    public static void NextPlayer(int player)
    {
        EventManager.Trigger(Events.NEXT_PLAYER, EventArgs.Empty);

        PlayerManager.NextPlayer(player);
    }

    private void OnPlay(EventArgs args)
    {
        if (State.ActivePlayer == null || !State.ActivePlayer.IsHuman) return;

        PlayingCard card;
        var marker = (IntEventArgs)args;

        if (Type == GameType.Simulation)
        {
            card = (PlayingCard)Utils.ChooseCard(State.ActivePlayer.Hand, State.LeadSuit);
            if (State.CurrentTrick.Plays.Count % State.PlaysPerTrick == 0)
            {
                EventManager.Trigger(Events.CLEAN_TABLE, System.EventArgs.Empty);
            }
            EventManager.Trigger(Events.AGENT_CARD, new PlayEventArgs(card.ToString(), Instance.State.ActivePlayer.Id));
        }
        else
        {
            if (!State.Deck.Exists(c => ((PlayingCard)c).BackId == marker.Value)) return;
            card = (PlayingCard)State.Deck.Find(c => ((PlayingCard)c).BackId == marker.Value);
        }

        State.Deck.Remove(card);
        Play(card, "UNKOWN");
    }

    private void onScreenPlay(EventArgs args)
    {
        if (State.ActivePlayer == null) return;

        var cardInString = ((PlayEventArgs)args).Value;
        var rank = (Rank)Enum.Parse(typeof(Rank), cardInString[0].ToString());
        var suit = (Suit)Enum.Parse(typeof(Suit), cardInString[1].ToString());

        var card = Instance.State.ActivePlayer.Hand.Find(c => c.Type.Equals(rank) && c.Symbol.Equals(suit));
        var found = Instance.State.ActivePlayer.Hand.Remove(card);
        Debug.Log("found: " + found);

        if (State.CurrentTrick.Plays.Count % State.PlaysPerTrick == 0)
        {
            EventManager.Trigger(Events.CLEAN_TABLE, System.EventArgs.Empty);
        }
        EventManager.Trigger(Events.AGENT_CARD, new PlayEventArgs(card.ToString(), Instance.State.ActivePlayer.Id));

        State.Deck.Remove(card);
        Play(card, "UNKOWN");
    }

    private void Play(ICard card, string playInfo)
    {
        var rank = (SuecaTypes.Rank)Enum.Parse(typeof(SuecaTypes.Rank), card.Type.ToString());
        var suit = (SuecaTypes.Suit)Enum.Parse(typeof(SuecaTypes.Suit), card.Symbol.ToString());


        State.ActivePlay = new Play(State.ActivePlayer.Id, card);
        PlayerManager.Play(State.ActivePlayer.Id, JsonConvert.SerializeObject(new SuecaTypes.Card(rank, suit)), playInfo);


        Rules.Validate();

        if (Type == GameType.Simulation)
        {

            if (State.HasRenounced)
            {
                Renounce();
            }
            else if (State.Tricks.Count == State.NumberOfTricks)
            {
                var lastTrick = State.Tricks.Last();
                var points = lastTrick.Plays.Sum(play =>
                {
                    var content = (PlayingCard)play.Content;
                    return content.Value;
                });

                PlayerManager.TrickEnd(lastTrick.Winner.Id, points);
                EventManager.Trigger(Events.TRICK_END, EventArgs.Empty);

                EventManager.Trigger(Events.GAME_END, EventArgs.Empty);
            }
            else if (State.CurrentTrick.Plays.Count == State.PlaysPerTrick)
            {
                var lastTrick = State.Tricks.Last();
                var points = lastTrick.Plays.Sum(play =>
                {
                    var content = (PlayingCard)play.Content;
                    return content.Value;
                });

                EventManager.Trigger(Events.TRICK_END, EventArgs.Empty);
                PlayerManager.TrickEnd(lastTrick.Winner.Id, points);

                OnNextTrick(EventArgs.Empty);
            }
            else
            {
                NextPlayer(State.ActivePlayer.Id);
            }
        }
        else
        {
            if (State.HasRenounced)
            {
                EventManager.Trigger(Events.TRICK_END, EventArgs.Empty);
                PlayerManager.Renounce(State.ActivePlayer.Id);
            }
            else if (State.CurrentTrick.Plays.Count == State.PlaysPerTrick)
            {
                var lastTrick = State.Tricks.Last();
                var points = lastTrick.Plays.Sum(play =>
                {
                    var content = (PlayingCard)play.Content;
                    return content.Value;
                });

                EventManager.Trigger(Events.TRICK_END, EventArgs.Empty);
                PlayerManager.TrickEnd(lastTrick.Winner.Id, points);
            }
            else
            {
                NextPlayer(State.ActivePlayer.Id);
            }
        }
    }

    public static void ProcessCard(ICard parsedCard, string playInfo)
    {
        var card = Instance.State.ActivePlayer.Hand.Find(c => c.Type.Equals(parsedCard.Type) && c.Symbol.Equals(parsedCard.Symbol));

        Instance.State.ActivePlayer.Hand.Remove(card);
        if (Instance.State.CurrentTrick.Plays.Count % Instance.State.PlaysPerTrick == 0)
        {
            EventManager.Trigger(Events.CLEAN_TABLE, System.EventArgs.Empty);
        }
        EventManager.Trigger(Events.AGENT_CARD, new PlayEventArgs(card.ToString(), Instance.State.ActivePlayer.Id));

        Instance.Play(card, playInfo);

    }

    private void OnRenounce(EventArgs args)
    {
        Renounce();
    }

    private void OnGameEnd(EventArgs args)
    {
        PlayerManager.GameEnd();
        PlayerManager.AccumulateScores();

        EndOrContinueGame();
    }

    private void OnNextTrick(EventArgs args)
    {
        var lastTrick = State.Tricks.Last();

        State.CurrentTrick.Plays.Clear();
        NextPlayer(lastTrick.Winner.Id);
    }

    public void StartGame()
    {

        //if (Type == GameType.Simulation)
        //{

        //}
        PlayerManager.GameStart(CurrentGame, State.TrumpCard);
        UIManager.SetActiveView("GameScreen");

        EventManager.Trigger(Events.NEXT_PLAYER, EventArgs.Empty);
        PlayerManager.NextPlayer(State.ActivePlayer.Id);
    }

    public void StartSession()
    {
        List<int> agentsIds = new List<int>();
        for (int i = 0; i < 4; i++)
        {
            IPlayer p = PlayerManager.GetPlayer(i);
            if (!p.IsHuman)
            {
                agentsIds.Add(p.Id);
            }
        }

        PlayerManager.SessionStart(SessionId, NumberOfRounds, agentsIds.ToArray(), PlayerManager.GetFloorId());
        Debug.Log("number of rounds sent in sessionStart " + NumberOfRounds);
        CurrentGame = 0;

        if (Type == GameType.Simulation)
        {
            StartGame();
            UIManager.SetActiveView("GameScreen");
        }
        else
        {
            PlayerManager.Shuffle();
            UIManager.SetActiveView("ShuffleScreen");
        }
    }

    public void NextRound()
    {
        CurrentGame++;

        InitGameState();
        State.ActivePlayer = PlayerManager.NextLeadPlayer();

        if (Type == GameType.Simulation)
        {
            StartGame();
        }
        else
        {
            PlayerManager.Shuffle();
            UIManager.SetActiveView("ShuffleScreen");
        }
    }

    public void EndSession()
    {
        UIManager.SetActiveView("StartScreen");
    }

    public void SetTrump(Suit suit)
    {
        State.Trumps = suit;
        EventManager.Trigger("OnTrump", new StringEventArgs(suit.ToString()));

        StartGame();
    }

    public void TrumpSet()
    {
        EventManager.Trigger("OnTrump", new StringEventArgs(State.TrumpCard.ToString()));
        PlayerManager.TrumpCard(Instance.State.TrumpCard);
        StartGame();
    }

    public void SetLeadPlayer(int playerId)
    {
        PlayerManager.SetLeadPlayer(playerId);
        State.ActivePlayer = State.Players.Find(p => p.Id == playerId);
    }

    public void SetRoundsFromSlider(Slider slider)
    {
        NumberOfRounds = (int) slider.value;
        CurrentGame = 0;
    }

    public void SetRounds(int numberOfGames)
    {
        NumberOfRounds = numberOfGames;
        CurrentGame = 0;
    }

    public void TootgleShouldGreet()
    {
        if (ShouldGreet == 1)
        {
            ShouldGreet = 0;
        }
        else
        {
            ShouldGreet = 1;
        }
    }

    public void Shuffle()
    {
        //PlayerManager.Cut();
        //UIManager.SetActiveView("CutScreen");
        PlayerManager.Deal();
        UIManager.SetActiveView("DealScreen");
    }

    public void Cut()
    {
        PlayerManager.Deal();
        UIManager.SetActiveView("DealScreen");
    }

    public void Deal()
    {
        if (Type == GameType.WithAgent || Type == GameType.TwoAgentsOpponents || Type == GameType.TwoAgentsTeam)
        {
            IPlayer firstAgentWithoutCards = PlayerManager.Instance.Players.Find(p => !p.IsHuman && (p.Hand == null || p.Hand.Count == 0));
            int agentId = firstAgentWithoutCards.Id;
            PlayerManager.ReceiveRobotCards(agentId);
            UIManager.SetActiveView("Robot1CardsScreen");
        }
        else if (Type == GameType.OnlyHumans)
        {
            UIManager.SetActiveView("TrumpCardScreen");
        }
    }

    public Sueca State { get; private set; }


    public static void ReceivePlayerCard(int playerId, int marker)
    {
        if (!Instance.State.Deck.Exists(c => ((PlayingCard)c).FrontId == marker)) return;

        var card = (PlayingCard)Instance.State.Deck.Find(c => ((PlayingCard)c).FrontId == marker);
        var wasAdded = PlayerManager.AddPlayerCard(playerId, card);

        if (wasAdded)
        {
            Instance.State.Deck.Remove(card);
            EventManager.Trigger(Events.AGENT_CARD_READ, new IntEventArgs(marker));
        }

    }

    public static void ReceiveAgentTrumpCard(int playerId, int marker)
    {
        if (!Instance.State.Deck.Exists(c => ((PlayingCard)c).BackId == marker)) return;

        var card = (PlayingCard)Instance.State.Deck.Find(c => ((PlayingCard)c).BackId == marker);
        var wasAdded = PlayerManager.AddPlayerTrumpCard(playerId, card);

        if (wasAdded)
        {
            Instance.State.Deck.Remove(card);
            Instance.State.TrumpCard = card;
            Instance.State.Trumps = card.Suit;
            EventManager.Trigger(Events.AGENT_CARD_READ, new IntEventArgs(marker));
            PlayerManager.TrumpCard(Instance.State.TrumpCard);
        }

    }

    public static void ReceiveTrumpCard(int marker)
    {
        if (!Instance.State.Deck.Exists(c => ((PlayingCard)c).BackId == marker)) return;

        var card = (PlayingCard)Instance.State.Deck.Find(c => ((PlayingCard)c).BackId == marker);
        Debug.Log("GameManager.ReceiveTrumpCard");
        Instance.State.TrumpCard = card;
        Instance.State.Trumps = card.Suit;
        EventManager.Trigger(Events.TRUMP_CARD_READ, EventArgs.Empty);
        PlayerManager.TrumpCard(Instance.State.TrumpCard);
    }

    private static void EndOrContinueGame()
    {
        //var scoreA = PlayerManager.Instance.TeamAAccumulateScore;
        //var scoreB = PlayerManager.Instance.TeamBAccumulateScore;

        //if (scoreA < Instance._numberOfRounds && scoreB < Instance._numberOfRounds)
        Debug.Log("currentgame " + Instance.CurrentGame + " rounds# " + Instance.NumberOfRounds);
        if (Instance.CurrentGame < Instance.NumberOfRounds - 1)
        {
            UIManager.SetActiveView("NextGameScreen");
        }
        else
        {
            UIManager.SetActiveView("EndScreen");
            PlayerManager.SessionEnd(GameManager.Instance.SessionId);

            Instance.CurrentGame = 0;
            Instance.NumberOfRounds = 3;
        }
    }

    private void Renounce()
    {
        var player = (ITeamPlayer)State.ActivePlayer;
        var team = player.Team;

        if (team.Name == "A")
        {
            PlayerManager.Instance.TeamBAccumulateScore += 4;

            // Distribute points to each team player
            PlayerManager.Instance.TeamA.Score = 0;
            PlayerManager.Instance.TeamB.Score = 60;
        }
        else
        {
            PlayerManager.Instance.TeamAAccumulateScore += 4;

            // Distribute points to each team player
            PlayerManager.Instance.TeamA.Score = 60;
            PlayerManager.Instance.TeamB.Score = 0;
        }

        PlayerManager.GameEnd();
        EndOrContinueGame();
    }

    public static void StartOver()
    {
        Instance.InitGameState();
        Instance.State.ActivePlayer = PlayerManager.NextLeadPlayer();
    }   

    public void InitSession1()
    {
        SessionId = 1;
        InitHumansWithTwoAgentsTeam();
    }

    public void InitSession2()
    {
        SessionId = 2;
        InitHumansWithTwoAgents((int)LeadPlayerChooser.PlayerPosition.Left);
    }

    public void InitSession3()
    {
        SessionId = 3;
        InitHumansWithTwoAgents((int)LeadPlayerChooser.PlayerPosition.Top);
    }

    public void SetP0Name(string arg)
    {
        Debug.Log("P0 name: " + arg);
        player0Name = arg;
    }

    public void SetP1Name(string arg)
    {
        Debug.Log("P1 name: " + arg);
        player1Name = arg;
    }

    public void InitSimulation(int leadPlayer = (int)LeadPlayerChooser.PlayerPosition.Bottom)
    {
        Type = GameType.Simulation;
        PlayerManager.InitWithHumans();
        //SetRounds(3);
        InitGameState();
        SetLeadPlayer(leadPlayer);
        //StartGame();
        UIManager.SetActiveView("StartScreen");
    }

    public void InitSimulationWithAgent(int leadPlayer = (int)LeadPlayerChooser.PlayerPosition.Bottom)
    {
        Type = GameType.Simulation;
        PlayerManager.InitHumansWithAgent();
        //SetRounds(3);
        InitGameState();
        SetLeadPlayer(leadPlayer);
        //StartGame();
        UIManager.SetActiveView("StartScreen");
    }

    public void InitHumans(int leadPlayer = (int)LeadPlayerChooser.PlayerPosition.Bottom)
    {
        Type = GameType.OnlyHumans;
        PlayerManager.InitWithHumans();
        //SetRounds(3);
        InitGameState();
        SetLeadPlayer(leadPlayer);
        //StartGame();
        UIManager.SetActiveView("StartScreen");
    }

    public void InitHumansWithAgent(int leadPlayer = (int)LeadPlayerChooser.PlayerPosition.Bottom)
    {
        Type = GameType.WithAgent;
        PlayerManager.InitHumansWithAgent();
        //SetRounds(3);
        InitGameState();
        SetLeadPlayer(leadPlayer);
        //StartGame();
        UIManager.SetActiveView("StartScreen");
    }

    public void InitSimulationWithTwoAgentsTeam(int leadPlayer = (int)LeadPlayerChooser.PlayerPosition.Bottom)
    {
        Type = GameType.Simulation;
        PlayerManager.InitHumansWithTwoAgentsTeam();
        //SetRounds(3);
        InitGameState();
        SetLeadPlayer(leadPlayer);
        //StartGame();
        UIManager.SetActiveView("StartScreen");
    }

    public void InitHumansWithTwoAgentsTeam(int leadPlayer = (int)LeadPlayerChooser.PlayerPosition.Bottom)
    {
        Type = GameType.TwoAgentsTeam;
        PlayerManager.InitHumansWithTwoAgentsTeam();
        //SetRounds(3);
        InitGameState();
        SetLeadPlayer(leadPlayer);
        //StartGame();
        UIManager.SetActiveView("StartScreen");
    }

    public void InitSimulationWithTwoAgents(int leadPlayer = (int)LeadPlayerChooser.PlayerPosition.Bottom)
    {
        Type = GameType.Simulation;
        PlayerManager.InitHumansWithTwoAgents();
        //SetRounds(3);
        InitGameState();
        SetLeadPlayer(leadPlayer);
        //StartGame();
        UIManager.SetActiveView("StartScreen");
    }

    public void InitHumansWithTwoAgents(int leadPlayer = (int)LeadPlayerChooser.PlayerPosition.Bottom)
    {
        Type = GameType.TwoAgentsOpponents;
        PlayerManager.InitHumansWithTwoAgents();
        //SetRounds(3);
        InitGameState();
        SetLeadPlayer(leadPlayer);
        //StartGame();
        UIManager.SetActiveView("StartScreen");
    }

    public void ResetTrick()
    {
        if (State.CurrentTrick.Plays.Count == 0) return;

        var trickFirstPlayerId = State.CurrentTrick.Plays[0].PlayerId;
        foreach (var play in State.CurrentTrick.Plays)
        {
            var player = PlayerManager.GetPlayer(play.PlayerId);
            var card = (ICard)play.Content;
            if (Type == GameType.Simulation || !player.IsHuman)
            {
                Debug.Log(string.Format("GameManager.ResetTrick: player {0} had {1} cards", player.Id, player.Hand.Count));
                player.Hand.Add(card);
                Debug.Log(string.Format("GameManager.ResetTrick: player {0} now has {1} cards", player.Id, player.Hand.Count));
            }
            else
            {
                Debug.Log(string.Format("GameManager.ResetTrick: deck had {1} cards", player.Id, player.Hand.Count));
                State.Deck.Add(card);
                Debug.Log(string.Format("GameManager.ResetTrick: deck now has {1} cards", player.Id, player.Hand.Count));
            }
        }

        if (State.CurrentTrick.Plays.Count == State.PlaysPerTrick)
        {
            var lastTrick = State.Tricks.Last();
            State.Tricks.Remove(lastTrick);
        }

        State.CurrentTrick.Plays.Clear();
        State.ActivePlayer = PlayerManager.GetPlayer(trickFirstPlayerId);

        // Notify agents that the current trick has been reset
        // PlayerManager.ResetTrick();

        PlayerManager.NextPlayer(trickFirstPlayerId);

        EventManager.Trigger(Events.NEXT_PLAYER, EventArgs.Empty);
    }
}
