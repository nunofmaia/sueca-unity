﻿using System.Collections.Generic;
using Shuffle;

public class Sueca : ITrickCardGame
{

    private Dictionary<HistoryEntry, bool> _missingSuitHistory;

    public struct HistoryEntry
    {
        public Suit Suit;
        public int PlayerId;

        public HistoryEntry(int playerId, Suit suit)
        {
            PlayerId = playerId;
            Suit = suit;
        }

        public override string ToString()
        {
            return string.Format("[HistoryEntry, {0} {1}]", PlayerId, Suit);
        }
    }
    public Sueca()
    {
        CurrentTrick = new Trick(new List<Play>());
        Tricks = new List<Trick>();
        Deck = new List<ICard>();
        HasRenounced = false;

        _missingSuitHistory = new Dictionary<HistoryEntry, bool>();
        for (var i = 0; i < 4; i++)
        {
            _missingSuitHistory.Add(new HistoryEntry(i, Suit.Clubs), false);
            _missingSuitHistory.Add(new HistoryEntry(i, Suit.Diamonds), false);
            _missingSuitHistory.Add(new HistoryEntry(i, Suit.Hearts), false);
            _missingSuitHistory.Add(new HistoryEntry(i, Suit.Spades), false);
        }
    }

    public Dictionary<HistoryEntry, bool> History { get { return _missingSuitHistory; } }

    public bool HasRenounced { set; get; }

    public Suit? Trumps { set; get; }

    public ICard TrumpCard { set; get; }

    public Suit? LeadSuit { set; get; }

    public List<ICard> Deck { set; get; }

    public List<IPlayer> Players { get { return PlayerManager.Instance.Players; } }

    public int NumberOfTricks { get { return 10; } }

    public int PlaysPerTrick { get { return 4; } }

    public IPlayer TrickWinner { set; get; }

    public Play ActivePlay { set; get; }

    public IPlayer ActivePlayer { set; get; }

    public List<Trick> Tricks { set; get; }

    public Trick CurrentTrick { set; get; }
}
