﻿using Shuffle;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayAgentCards : MonoBehaviour
{

    private Image[] _cards;
    private IPlayer _agent;
    private int _numberOfCards;
    public int PlayerId;

    void Awake()
    {
        //EventManager.Listen(Events.AGENT_CARD, OnCard);
        _cards = GetComponentsInChildren<Image>();
        _numberOfCards = _cards.Length;
        _agent = PlayerManager.Instance.Players.Find(p => p.Id == PlayerId);
    }

    void OnEnable()
    {
        if (_cards != null && _agent != null)
        {
            int i = 0;
            foreach (var card in _cards)
            {
                if (_agent.IsHuman)
                {
                    card.enabled = false;
                    Text[] cardLabels = card.GetComponentsInChildren<Text>();
                    foreach (var label in cardLabels)
                    {
                        label.enabled = false;
                    }
                }
                else
                {
                    card.enabled = true;
                    if (_agent.CanChoosePlayOnScreen)
                    {
                        var suits = new Dictionary<char, Color>() { { '\u2660', Color.white }, { '\u2665', Color.red }, { '\u2663', Color.white }, { '\u2666', Color.red } };
                        var color = suits[_agent.Hand[i].ToString()[1]];

                        AgentCard _card = card.GetComponentInChildren<AgentCard>();

                        _card.Rank1.text = _agent.Hand[i].ToString()[0].ToString();
                        _card.Rank2.text = _agent.Hand[i].ToString()[0].ToString();
                        _card.Suit1.text = _agent.Hand[i].ToString()[1].ToString();
                        _card.Suit2.text = _agent.Hand[i].ToString()[1].ToString();

                        _card.Suit1.color = color;
                        _card.Suit2.color = color;

                        _card.PlayerId = PlayerId;
                    }
                    else
                    {
                        AgentCard _card = card.GetComponentInChildren<AgentCard>();
                        _card.Rank1.enabled = false;
                        _card.Rank2.enabled = false;
                        _card.Suit1.enabled = false;
                        _card.Suit2.enabled = false;
                    }
                }
                i++;
            }
            _numberOfCards = 10;
        }
    }

    void Start()
    {
    }

    void Update()
    {
        if (_agent == null) return;

        if (!_agent.IsHuman && !_agent.CanChoosePlayOnScreen)
        {
            var nCards = _agent.Hand.Count;
            var diff = _numberOfCards - nCards;

            if (diff == 0) return;

            for (var i = 10; i > 0; i--)
            {
                if (_numberOfCards - i < diff)
                {
                    _cards[i - 1].enabled = false;
                }
                else
                {
                    _cards[i - 1].enabled = true;
                }
            }
        }
    }

    void OnDisable()
    {
        foreach (var card in _cards)
        {
            if (_agent.IsHuman)
            {
                card.enabled = false;
            }
            else
            {
                card.enabled = true;
            }
        }
    }
}
