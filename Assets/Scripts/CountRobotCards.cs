﻿using Shuffle;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class CountRobotCards : MonoBehaviour
{
    public Text CounterLabel;
    public Button ContinueButton;
    public int NumberOfCards;

    private int _counter;

    void Start()
    {
        ContinueButton.interactable = false;
        CounterLabel.text = _counter.ToString();
        EventManager.Listen(Events.AGENT_CARD_READ, UpdateCounter);
        EventManager.Listen(Events.NO_CARDS, UpdateButton);

        ContinueButton.onClick.AddListener(ChangeScene);
    }

    void UpdateCounter(EventArgs eventArgs)
    {
        _counter++;
        CounterLabel.text = _counter.ToString();
        
        if (_counter < NumberOfCards) return;

        //StartCoroutine(Continue());
    }

    void UpdateButton(EventArgs eventArgs)
    {
        if (_counter == NumberOfCards)
        {
            ContinueButton.interactable = true;
        }
    }
    
    IEnumerator Continue()
    {
        yield return new WaitForSeconds(10);
        ContinueButton.interactable = true;
    }

    void ChangeScene()
    {
        IPlayer nextAgentWithoutCards = PlayerManager.Instance.Players.Find(p => !p.IsHuman && (p.Hand == null || p.Hand.Count == 0));
        if (nextAgentWithoutCards != null)
        {
            UIManager.SetActiveView("Robot2CardsScreen");
        }
        else if (PlayerManager.Instance.Players[PlayerManager.Instance.DealPlayerId].IsHuman)
        {
            UIManager.SetActiveView("TrumpCardScreen");
        }
        else
        {
            GameManager.Instance.StartGame();
        }
    }

    void OnEnable()
    {
        _counter = 0;
        ContinueButton.interactable = false;
        CounterLabel.text = _counter.ToString();

    }
}