﻿using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public List<GameObject> Views;

    private GameObject _currentView;
    public static UIManager Instance { private set; get; }

    void Awake()
    {
        Instance = this;
    }

    public static void ResetViews()
    {
        foreach (var view in Instance.Views)
        {
            view.SetActive(false);
        }
    }

    public static void SetActiveView(string viewName)
    {
        var newView = Instance.Views.Find(v => v.name == viewName);
        if (newView == null) return;

        if (Instance._currentView != null) Instance._currentView.SetActive(false);

        Instance._currentView = newView;
        Instance._currentView.SetActive(true);
    }

    public static string CurrentView
    {
        get { return Instance._currentView != null ? Instance._currentView.name : ""; }
    }
}
