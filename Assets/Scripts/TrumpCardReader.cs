﻿using System;
using UnityEngine;

[RequireComponent(typeof(MarkerReaderUI))]
public class TrumpCardReader : MonoBehaviour
{
    private MarkerReaderUI _reader;
    private int noCardCounter;

    void Awake()
    {
        _reader = GetComponent<MarkerReaderUI>();
        noCardCounter = 0;
    }

    void onEnable()
    {
        noCardCounter = 0;
    }

    void Update()
    {

        if (_reader.Markers.Count == 0)
        {
            noCardCounter++;
            if (noCardCounter == 50)
            {
                EventManager.Trigger(Events.NO_CARDS, EventArgs.Empty);
            }
        }
        foreach (var marker in _reader.Markers)
        {
            noCardCounter = 0;
            GameManager.ReceiveTrumpCard(marker.getSymbolID());
        }
    }

    void onDisable()
    {
        noCardCounter = 0;
    }
}
