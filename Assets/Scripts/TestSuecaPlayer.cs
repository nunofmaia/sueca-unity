﻿using System.Collections.Generic;
using Shuffle;
using UnityEngine;

public class TestSuecaPlayer : Shuffle.ITeamPlayer
{
    public TestSuecaPlayer(int id, string name, ITeam team)
    {
        Id = id;
        Name = name;
        Team = team;
        Score = 0;
    }

    public ITeam Team { get; private set; }

    public int Id { get; private set; }

    public string Name { get; set; }

    public List<Shuffle.ICard> Hand { get; set; }

    public int Score { get; set; }

    public override string ToString()
    {
        return string.Format("[Player {0}: {1} cards, {2} points]", Id, Hand.Count, Score);
    }


    public bool IsHuman { get { return true; } }

    public bool CanChoosePlayOnScreen { get { return false; } }

    public void Dispose()
    {
        
    }

    public void Play(int player, string card, string playInfo)
    {
        
    }

    public void GameStart(int gameId, int playerId, int teamId, ICard trumpCard, int trumpCardPlayer, List<ICard> cards, string partnerName, int floorId)
    {

    }

    public void GameEnd(int team1Score, int team2Score)
    {
        
    }

    public void NextPlayer(int playerId)
    {
        
    }

    public void SessionStart(int sessionId, int numGames, int[] agentsIds)
    {
        
    }

    public void SessionEnd(int team0Score, int team1Score)
    {
        
    }

    public void Shuffle(int playerId)
    {
        
    }

    public void Deal(int playerId)
    {
        
    }

    public void Cut(int playerId)
    {
        
    }

    public void TrumpCard(ICard trumpCard, int playerId)
    {
        
    }

    public void ReceiveRobotCards(int playerId)
    {
        
    }

    public void TrickEnd(int winnerId, int trickPoints)
    {
        
    }

    public void Renounce(int playerId)
    {
        
    }

    public void GlanceAtScreen(double x, double y)
    {
        
    }

    public void GazeAtScreen(double x, double y)
    {
        
    }

    public void TargetScreenInfo(string targetName, int x, int y)
    {
        
    }

}
