﻿using UnityEngine;

namespace Flow
{
    [System.Serializable]
    public class Node : ScriptableObject, IValidator
    {
        public string title;
        public Rect rect;
        public Color color;
        public bool isRoot;
        public string texture;

        public Node()
        {
            title = this.GetType().ToString();
            color = Color.gray;
            isRoot = false;
            // texture = "Resources.Load("circle") as Texture;"
            texture = "circle";
        }

        public virtual void Draw()
        {
        }

        public virtual void DrawCurves()
        {
        }

        public virtual void SetChild(Node node, Vector2 clickPosition)
        {
        }

        public virtual Node ClickedOnNode(Vector2 position)
        {
            return null;
        }

        public virtual void NodeDeleted(Node node)
        {
        }

        public virtual void Validate()
        {
            Debug.Log(string.Format("{0}: node validation", title));
        }

        public override string ToString()
        {
            return string.Format("[{0}]", title);
        }
    }
}