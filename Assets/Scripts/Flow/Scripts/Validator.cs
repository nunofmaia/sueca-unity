﻿using UnityEngine;

namespace Flow
{
    public interface IValidator
    {
        void Validate();
    }

    public class Validator : MonoBehaviour
    {
        public Flowchart chart;

        void Start()
        {
            if (chart)
            {
                chart.Validate();
            }
        }
    }
}