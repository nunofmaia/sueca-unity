﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Reflection;
using System;
using System.Linq;
using Flow;

public class FlowchartEditor : EditorWindow
{

    const int NODE_WIDTH = 50;
    const int NODE_HEIGHT = 50;
    
    public static class ReflectiveEnumerator
    {
        static ReflectiveEnumerator()
        {
        }

        public static IEnumerable<Type> GetEnumerableOfType<T>(params object[] args) where T : class
        {
            List<Type> objects = new List<Type>();
            foreach (Type type in
                     Assembly.GetAssembly(typeof(T)).GetTypes()
                     .Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(typeof(T))))
            {
                objects.Add(type);
            }

            return objects;
        }
    }

    Vector2 mousePosition;

    Node selectedNode;
    bool onTransitionMode = false;

    GenericMenu contextMenu = new GenericMenu();
    Flowchart chart = ScriptableObject.CreateInstance<Flowchart>() as Flowchart;
    string defaultPath = "Assets/Rules/";
    string filename = "";
    
    Color lightColor = new Color(0.87f, 0.87f, 0.87f);
    Color darkColor = new Color(0.4f, 0.4f, 0.4f);
    Texture2D background;
    bool isLightTheme = true;
    Texture rootNodeTexture = Resources.Load("source") as Texture;

    [MenuItem("Window/Flowchart Editor #&f")]
    static void ShowEditor()
    {
        FlowchartEditor editor = EditorWindow.GetWindow<FlowchartEditor>();
        editor.title = "Flowchart Editor";
    }

    void OnDestroy()
    {
        if (IsAssetPersistent(chart))
        {
            EditorUtility.SetDirty(chart);
        }
    }

    void OnGUI()
    {
        
        // Draw window background
        if (isLightTheme)
        {
            background = MakeTextureWithColor(lightColor);
        }
        else
        {
            background = MakeTextureWithColor(darkColor);            
        }
        GUI.DrawTexture(new Rect(0, 0, maxSize.x, maxSize.y), background, ScaleMode.StretchToFill);
        
        Event e = Event.current;
        mousePosition = e.mousePosition;

        bool clickedOnNode = false;
        int selectedIndex = -1;

        for (int i = 0; i < chart.nodes.Count; i++)
        {
            if (chart.nodes[i].rect.Contains(mousePosition))
            {
                clickedOnNode = true;
                selectedIndex = i;
                break;
            }
        }

        if (e.button == 1 && e.type == EventType.MouseDown && !onTransitionMode)
        {
            ShowContextMenu(clickedOnNode);
            e.Use();
        }

        if (e.button == 0 && e.type == EventType.MouseDown && onTransitionMode)
        {
            if (clickedOnNode && !chart.nodes[selectedIndex].Equals(selectedNode))
            {
                selectedNode.SetChild(chart.nodes[selectedIndex], mousePosition);
                onTransitionMode = false;
                selectedNode = null;
            }

            if (!clickedOnNode)
            {
                onTransitionMode = false;
                selectedNode = null;
            }

            e.Use();
        }

        if (e.button == 0 && e.type == EventType.MouseDown && !onTransitionMode)
        {
            if (clickedOnNode)
            {
                Node nodeToChange = chart.nodes[selectedIndex].ClickedOnNode(mousePosition);
                if (nodeToChange != null)
                {
                    selectedNode = nodeToChange;
                    onTransitionMode = true;
                }
            }
        }

        if (onTransitionMode && selectedNode != null)
        {
            Rect mouseRect = new Rect(e.mousePosition.x, e.mousePosition.y, 10, 10);
            Rect rct = selectedNode.rect;
            rct.x += selectedNode.rect.width / 2;
            rct.y += selectedNode.rect.height / 2;
            rct.width = 1;
            rct.height = 1;
            DrawNodeCurves(rct, mouseRect, new Color(1, 1, 1, 0.25f));

            Repaint();
        }

        foreach (Node node in chart.nodes)
        {
            node.DrawCurves();
        }

        BeginWindows();

        for (int i = 0; i < chart.nodes.Count; i++)
        {
            Node node = chart.nodes[i];
            Texture texture = Resources.Load(node.texture) as Texture;
            GUIStyle style = StyleNode(texture, node.color, node.isRoot);
            style.fontSize = 30;
            style.contentOffset = new Vector2(0, -40);
            style.fontStyle = FontStyle.Normal;
            node.rect = GUI.Window(i, node.rect, DrawNode, node.title, style);
        }

        EndWindows();

        DrawToolbar();
    }

    void ShowContextMenu(bool clickedOnWindow)
    {
        if (!clickedOnWindow)
        {
            GenerateEditorContextMenu();
        }
        else
        {
            GenerateNodeActionsContextMenu();
        }

        contextMenu.ShowAsContext();
    }

    GUIStyle StyleNode(Texture texture, Color color, bool isRoot)
    {
        GUIStyle style = new GUIStyle();
        style.alignment = TextAnchor.UpperCenter;
        style.fontStyle = FontStyle.Bold;
        
        if (isRoot)
        {
            style.normal.background = rootNodeTexture as Texture2D;
        }
        else if (texture != null)
        {
            style.fixedHeight = NODE_HEIGHT;
            style.fixedWidth = NODE_WIDTH;
            style.normal.background = texture as Texture2D;
        }
        else
        {
            style.normal.background = MakeTextureWithColor(color);
        }

        return style;
    }

    Texture2D MakeTextureWithColor(Color color)
    {
        Texture2D tex = new Texture2D(1, 1);
        tex.SetPixel(1, 1, color);
        tex.Apply();

        return tex;
    }

    void DrawNode(int id)
    {
        chart.nodes[id].Draw();
        GUI.DragWindow();
    }

    void DrawToolbar()
    {
        EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
        
        isLightTheme = GUILayout.Toggle(isLightTheme, "Light", EditorStyles.toolbarButton);
        isLightTheme = !GUILayout.Toggle(!isLightTheme, "Dark", EditorStyles.toolbarButton);
        
        GUILayout.Space(6);

        if (GUILayout.Button("Clear", EditorStyles.toolbarButton))
        {
            ClearNodes();
        }

        chart = (Flowchart)EditorGUILayout.ObjectField(chart, typeof(Flowchart), false);

        GUI.enabled = !IsAssetPersistent(chart);
        filename = GUILayout.TextField(filename, 100, EditorStyles.toolbarTextField, GUILayout.Width(150));
        if (GUILayout.Button("Save as...", EditorStyles.toolbarButton))
        {
            CreateAsset();
        }

        GUI.enabled = true;
        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();
    }

    void CreateAsset()
    {
        AssetDatabase.CreateAsset(chart, defaultPath + filename + ".asset");

        chart.nodes.ForEach(n => CreateSubAsset(n));

        AssetDatabase.SaveAssets();
        Debug.Log("Asset saved");
    }

    void CreateSubAsset(Node node)
    {
        if (IsAssetPersistent(chart))
        {
            node.name = node.title;
            AssetDatabase.AddObjectToAsset(node, chart);
            AssetDatabase.SaveAssets();
        }
    }

    void AddNode(Node node)
    {
        if (chart != null)
        {
            chart.Add(node);
            CreateSubAsset(node);
        }
    }

    void DeleteSubAsset(Node node)
    {
        if (chart && AssetDatabase.IsSubAsset(node))
        {
            Debug.Log("Destroying subasset");
            chart.nodes.Remove(node);
            UnityEngine.Object.DestroyImmediate(node, true);
            AssetDatabase.SaveAssets();
        }

    }

    void ClearNodes()
    {
        Debug.Log(chart.nodes.Count + " nodes");
        while (chart.nodes.Count > 0)
        {
            Node n = chart.nodes.First();
            DeleteSubAsset(n);
        }
    }

    void GenerateEditorContextMenu()
    {
        contextMenu = new GenericMenu();
        foreach (var actionType in ReflectiveEnumerator.GetEnumerableOfType<Flow.Action>())
        {
            contextMenu.AddItem(new GUIContent("Actions/" + actionType.ToString()), false, ContextMenuCallback, actionType.Name);
        }

        foreach (var predicateType in ReflectiveEnumerator.GetEnumerableOfType<Predicate>())
        {
            contextMenu.AddItem(new GUIContent("Predicates/" + predicateType.ToString()), false, ContextMenuCallback, predicateType.Name);
        }

        foreach (var flowType in ReflectiveEnumerator.GetEnumerableOfType<Flow.Flow>())
        {
            contextMenu.AddItem(new GUIContent("Flows/" + flowType.ToString()), false, ContextMenuCallback, flowType.Name);
        }
    }

    void GenerateNodeActionsContextMenu()
    {
        contextMenu = new GenericMenu();
        contextMenu.AddItem(new GUIContent("Info"), false, NodeInfo);
        contextMenu.AddItem(new GUIContent("Toggle root"), false, ToggleRoot);
        contextMenu.AddItem(new GUIContent("Make transition"), false, MakeTransition);
        contextMenu.AddSeparator("");
        contextMenu.AddItem(new GUIContent("Delete Node"), false, DeleteNode);
    }

    void ToggleRoot()
    {
        bool clickedOnNode = false;
        int selectedIndex = -1;
        int currentRootIndex = -1;

        for (int i = 0; i < chart.nodes.Count; i++)
        {
            Node node = chart.nodes[i];

            if (node.isRoot)
            {
                currentRootIndex = i;
            }

            if (node.rect.Contains(mousePosition))
            {
                clickedOnNode = true;
                selectedIndex = i;
                break;
            }
        }

        if (clickedOnNode &&
            (currentRootIndex == -1 || currentRootIndex == selectedIndex))
        {
            chart.nodes[selectedIndex].isRoot = !chart.nodes[selectedIndex].isRoot;
        }
        else
        {
            Debug.Log("There is already a root set for the flowchart");
        }
    }

    void MakeTransition()
    {
        bool clickedOnNode = false;
        int selectedIndex = -1;

        for (int i = 0; i < chart.nodes.Count; i++)
        {
            if (chart.nodes[i].rect.Contains(mousePosition))
            {
                clickedOnNode = true;
                selectedIndex = i;
                break;
            }
        }

        if (clickedOnNode)
        {
            selectedNode = chart.nodes[selectedIndex];
            onTransitionMode = true;
        }
    }

    void NodeInfo()
    {
        int selectedIndex = -1;
        for (int i = 0; i < chart.nodes.Count; i++)
        {
            if (chart.nodes[i].rect.Contains(mousePosition))
            {
                selectedIndex = i;
                break;
            }
        }

        Debug.Log(chart.nodes[selectedIndex]);
    }

    void DeleteNode()
    {
        int selectedIndex = -1;
        bool clickedOnNode = false;
        for (int i = 0; i < chart.nodes.Count; i++)
        {
            if (chart.nodes[i].rect.Contains(mousePosition))
            {
                selectedIndex = i;
                clickedOnNode = true;
                break;
            }
        }

        Node deletedNode = chart.nodes[selectedIndex];

        if (clickedOnNode)
        {
            foreach (Node node in chart.nodes)
            {
                node.NodeDeleted(deletedNode);
            }
        }

        chart.nodes.RemoveAt(selectedIndex);
        if (IsAssetPersistent(chart))
        {
            DeleteSubAsset(deletedNode);
        }
    }
    
    bool IsAssetPersistent(UnityEngine.Object obj)
    {
        return AssetDatabase.GetAssetPath(obj) != "";    
    }

    void ContextMenuCallback(object obj)
    {
        string callback = obj.ToString();
        Node node = (Node)ScriptableObject.CreateInstance(callback);
        node.rect = new Rect(mousePosition.x, mousePosition.y, NODE_WIDTH, NODE_HEIGHT);
        AddNode(node);
    }

    public static void DrawNodeCurves(Rect start, Rect end, Color color)
    {
        Vector3 startPos = new Vector3(start.x, start.y);
        Vector3 endPos = new Vector3(end.x + end.width / 2, end.y + end.height / 2, 0);
        Vector3 startTan = startPos + Vector3.right; //startPos + Vector3.right * 50;
        Vector3 endTan = endPos + Vector3.left; //endPos + Vector3.left * 50;
        Color shadowCol = new Color(0.74f, 0.75f, 0.73f);

        Handles.DrawBezier(startPos, endPos, startTan, endTan, shadowCol, null, 7);
        Handles.DrawBezier(startPos, endPos, startTan, endTan, color, null, 5);
    }
}

#endif
