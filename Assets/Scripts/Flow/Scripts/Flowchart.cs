﻿using UnityEngine;
using System.Collections.Generic;

namespace Flow
{
    [System.Serializable]
    public class Flowchart : ScriptableObject, IValidator
    {
        public List<Node> nodes;

        Node root;

        void OnEnable()
        {
            if (nodes == null)
            {
                nodes = new List<Node>();
            }
        }

        public void Add(Node node)
        {
            nodes.Add(node);
        }

        public void Validate()
        {
            root = root ? root : nodes.Find(node => node.isRoot);

            if (root != null)
            {
                // Start the validation here
                root.Validate();
            }
        }
    }
}