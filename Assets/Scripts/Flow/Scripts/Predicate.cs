﻿using UnityEngine;

namespace Flow
{
    [System.Serializable]
    public abstract class Predicate : Node
    {
        public Node truthy;
        public Node falsy;
        protected bool condition;

        public Predicate()
        {
            color = Color.yellow;
            texture = "diamond";
        }

        public override void DrawCurves()
        {
            if (truthy)
            {
                DrawCurve(truthy, new Color(0.77f, 0.875f, 0.65f));
            }

            if (falsy)
            {
                DrawCurve(falsy, new Color(0.92f, 0.71f, 0.71f));
            }
        }

        void DrawCurve(Node child, Color color)
        {
            Rect rct = rect;
            rct.x += rect.width / 2;
            rct.y += rect.height / 2;
            rct.width = 1;
            rct.height = 1;
            
            #if UNITY_EDITOR
            FlowchartEditor.DrawNodeCurves(rct, child.rect, color);
            #endif
        }

        public override void NodeDeleted(Node node)
        {
            if (truthy && truthy.Equals(node))
            {
                truthy = null;
            }

            if (falsy && falsy.Equals(node))
            {
                falsy = null;
            }
        }

        public override Node ClickedOnNode(Vector2 position)
        {
            Node node = null;
        
            position.x -= rect.x;
            position.y -= rect.y;
        
            if (truthy && truthy.rect.Contains(position))
            {
                node = truthy;
                truthy = null;
            } else if (falsy && falsy.rect.Contains(position))
            {
                node = falsy;
                falsy = null;
            }
        
            return node;
        }

        public override void SetChild(Node node, Vector2 clickPosition)
        {
            if (node.rect.Contains(clickPosition))
            {
                if (truthy == null)
                {
                    truthy = node;
                } else if (falsy == null)
                {
                    falsy = node;
                }
            }
        }

        public override void Validate()
        {
            if (condition)
            {
                if (truthy)
                {
                    truthy.Validate();
                }
            } else
            {
                if (falsy)
                {
                    falsy.Validate();
                }
            }
        }

        public override string ToString()
        {
            return string.Format("[Predicate, hasChild={0}]", truthy != null || falsy != null);
        }
    }
}