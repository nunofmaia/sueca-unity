﻿using UnityEngine;

namespace Flow
{
    [System.Serializable]
    public abstract class Flow : Node
    {
        public Flowchart chart;
        public Node child;
        
        public Flow()
        {
            color = Color.cyan;
            texture = "flow";
        }
        
        public override void DrawCurves()
        {
            if (child)
            {
                Rect rct = rect;
                rct.x += rect.width / 2;
                rct.y += rect.height / 2;
                rct.width = 1;
                rct.height = 1;
                
                #if UNITY_EDITOR
                FlowchartEditor.DrawNodeCurves(rct, child.rect, Color.black);
                #endif
            }
        }
        
        public override void NodeDeleted(Node node)
        {
            if (child && child.Equals(node))
            {
                child = null;
            }
        }
        
        public override Node ClickedOnNode(Vector2 position)
        {
            Node node = null;

            position.x -= rect.x;
            position.y -= rect.y;

            if (child && child.rect.Contains(position))
            {
                node = child;
                child = null;
            }

            return node;
        }
        
        public override void SetChild(Node node, Vector2 clickPosition)
        {
            if (node.rect.Contains(clickPosition))
            {
                child = node;
            }
        }
        
        public override void Draw()
        {
            base.Draw();
            
            //  chart = EditorGUILayout.ObjectField(chart, typeof(Flowchart), false) as Flowchart;
        }
        
        public override void Validate()
        {
            chart.Validate();
            
            if (child)
            {
                child.Validate();
            }
        }
        
        public override string ToString()
        {
            return string.Format("[Flow, hasChild={0}]", child != null);
        }
    }
}
