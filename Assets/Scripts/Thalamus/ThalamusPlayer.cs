﻿using System;
using System.Collections.Generic;
using Shuffle;

public class ThalamusPlayer : ITeamPlayer
{
    private readonly ThalamusConnector _thalamusConnector;

    public ThalamusPlayer(int id, string name, ITeam team, ThalamusConnector thalamusConnector)
    {
        _thalamusConnector = thalamusConnector;
        Id = id;
        Name = name;
        Team = team;
        Score = 0;
    }

    public bool IsHuman { get { return false; } }

    public bool CanChoosePlayOnScreen { get { return false; } }

    public void Play(int player, string card, string playInfo)
    {
        //_thalamusConnector.Play(player, card);
    }

    public void NextPlayer(int player)
    {
        //_thalamusConnector.NextPlayer(player);
    }

    public void GameStart(int gameId, int playerId, int teamId, ICard trumpCard, int trumpCardPlayer, List<ICard> cards, string partnerName, int floorId)
    {
        int nCards = cards.Count;
        string[] hand = new string[nCards];

        for (var i = 0; i < nCards; i++)
        {
            var rank = (SuecaTypes.Rank) Enum.Parse(typeof(SuecaTypes.Rank), cards[i].Type.ToString());
            var suit = (SuecaTypes.Suit) Enum.Parse(typeof(SuecaTypes.Suit), cards[i].Symbol.ToString());

            var card = new SuecaTypes.Card(rank, suit);
            hand[i] = card.SerializeToJson();
        }

        var trumpRank = (SuecaTypes.Rank)Enum.Parse(typeof(SuecaTypes.Rank), trumpCard.Type.ToString());
        var trumpSuit = (SuecaTypes.Suit)Enum.Parse(typeof(SuecaTypes.Suit), trumpCard.Symbol.ToString());

        var trump = new SuecaTypes.Card(trumpRank, trumpSuit);
        var trumpSerialized = trump.SerializeToJson();
        if (_thalamusConnector != null)
        {
            _thalamusConnector.GameStart(gameId, playerId, teamId, trumpSerialized, trumpCardPlayer, hand, partnerName, floorId);
        }
    }

    public void SessionStart(int sessionId, int numGames, int[] agentsIds)
    {
        //_thalamusConnector.SessionStart(numGames, agentsIds);
    }

    public void SessionEnd(int team0Score, int team1Score)
    {
        //_thalamusConnector.SessionEnd(team0Score, team1Score);
    }

    public void Shuffle(int playerId)
    {
        //_thalamusConnector.Shuffle(playerId);
    }

    public void Deal(int playerId)
    {
        //_thalamusConnector.Deal(playerId);
    }

    public void Cut(int playerId)
    {
        //_thalamusConnector.Cut(playerId);
    }

    public void TrumpCard(ICard trumpCard, int playerId)
    {
        //var rank = (SuecaTypes.Rank)Enum.Parse(typeof(SuecaTypes.Rank), trumpCard.Type.ToString());
        //var suit = (SuecaTypes.Suit)Enum.Parse(typeof(SuecaTypes.Suit), trumpCard.Symbol.ToString());

        //var card = new SuecaTypes.Card(rank, suit);
        //var serializedCard = card.SerializeToJson();
        //_thalamusConnector.TrumpCard(serializedCard, playerId);
    }

    public void ReceiveRobotCards(int playerId)
    {
        //_thalamusConnector.ReceiveRobotCards();
    }

    public void TrickEnd(int winnerId, int trickPoints)
    {
        //_thalamusConnector.TrickEnd(winnerId, trickPoints);
    }

    public void Renounce(int playerId)
    {
        //_thalamusConnector.Renounce(playerId);
    }

    public void GameEnd(int team1Score, int team2Score)
    {
        //_thalamusConnector.GameEnd(team1Score, team2Score);
    }

    public void GazeAtScreen(double x, double y)
    {
        //_thalamusConnector.GazeAtScreen(x, y);
    }

    public void TargetScreenInfo(string targetName, int x, int y)
    {
        //_thalamusConnector.TargetScreenInfo(targetName, x, y);
    }

    public void GlanceAtScreen(double x, double y)
    {
        //_thalamusConnector.GlanceAtScreen(x, y);
    }

    public void Dispose()
    {
        //_thalamusConnector.Dispose();
    }

    public int Id { private set; get; }
    public string Name { get; set; }

    public List<ICard> Hand { get; set; }

    public int Score { get; set; }

    public ITeam Team { private set; get; }
}
