﻿using System.Collections.Generic;
using Shuffle;

public interface ISPerceptions
{
    void Dispose();
    void Play(int player, string card, string playInfo);
    void GameStart(int gameId, int playerId, int teamId, ICard trumpCard, int trumpCardPlayer, List<ICard> cards, string partnerName, int floorId);
    void GameEnd(int team1Score, int team2Score);
    void NextPlayer(int playerId);
    void SessionStart(int sessionId, int numGames, int[] agentsIds);
    void SessionEnd(int team0Score, int team1Score);
    void Shuffle(int playerId);
    void Deal(int playerId);
    void Cut(int playerId);
    void TrumpCard(ICard trumpCard, int playerId);
    void ReceiveRobotCards(int playerId);
    void TrickEnd(int winnerId, int trickPoints);
    void Renounce(int playerId);
    void GlanceAtScreen(double x, double y);
    void GazeAtScreen(double x, double y);
    void TargetScreenInfo(string targetName, int x, int y);
}
