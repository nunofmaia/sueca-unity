using CookComputing.XmlRpc;

public interface ISMessagesRpc : ISMessages, IXmlRpcProxy { }

public interface ISMessages
{
    void Dispose();

    [XmlRpcMethod]
    void Play(int player, string card, string playInfo, int floorId);

    [XmlRpcMethod]
    void GameStart(int gameId, int playerId, int teamId, string trumpCard, int trumpCardPlayer, string[] cards, string partnerName, int floorId);

    [XmlRpcMethod]
    void GameEnd(int team1Score, int team2Score, int floorId);

    [XmlRpcMethod]
    void NextPlayer(int playerId, int floorId);

    [XmlRpcMethod]
    void SessionStart(int sessionId, int numGames, int[] agentsIds, int floorId);

    [XmlRpcMethod]
    void SessionEnd(int sessionId, int team0Score, int team1Score, int floorId);

    [XmlRpcMethod]
    void Shuffle(int playerId, int floorId);

    [XmlRpcMethod]
    void Deal(int playerId, int floorId);

    [XmlRpcMethod]
    void Cut(int playerId, int floorId);

    [XmlRpcMethod]
    void TrumpCard(string trumpCard, int playerId, int floorId);

    [XmlRpcMethod]
    void ReceiveRobotCards(int playerId, int floorId);

    [XmlRpcMethod]
    void TrickEnd(int winnerId, int trickPoints, int floorId);

    [XmlRpcMethod]
    void Renounce(int playerId, int floorId);

    [XmlRpcMethod]
    void GlanceAtScreen(double x, double y);

    [XmlRpcMethod]
    void GazeAtScreen(double x, double y);

    [XmlRpcMethod]
    void TargetScreenInfo(string targetName, int x, int y);

}