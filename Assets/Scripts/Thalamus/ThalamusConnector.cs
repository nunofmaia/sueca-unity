﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using CookComputing.XmlRpc;
using UnityEngine;

public interface IThalamusSActions
{
    [XmlRpcMethod]
    void Play(int player, string card, string playInfo);
}

public class ThalamusListener : XmlRpcListenerService, IThalamusSActions
{
    private readonly PlayerManager _manager;

    public ThalamusListener(PlayerManager manager)
    {
        _manager = manager;
    }

    public void Play(int player, string card, string playInfo)
    {
        var action = new QueuedAction(player, card, playInfo);

        Debug.Log("[ThalamusConnector] Play " + player + "," + card + "," + playInfo);
        _manager.Queue(action);
    }
}

public class ThalamusConnector : ISMessages
{
    private string _remoteAddress = "localhost";

    private bool _printExceptions = true;
    public string RemoteAddress
    {
        get { return _remoteAddress; }
        set
        {
            _remoteAddress = value;
            _remoteUri = string.Format("http://{0}:{1}/", _remoteAddress, _remotePort);
            _rpcProxy.Url = _remoteUri;
        }
    }

    private int _remotePort = 7000;
    public int RemotePort
    {
        get { return _remotePort; }
        set
        {
            _remotePort = value;
            _remoteUri = string.Format("http://{0}:{1}/", _remoteAddress, _remotePort);
            _rpcProxy.Url = _remoteUri;
        }
    }

    private HttpListener _listener;
    private bool _serviceRunning;
    private int _localPort = 7001;
    private bool _shutdown;
    List<HttpListenerContext> _httpRequestsQueue = new List<HttpListenerContext>();
    private Thread _dispatcherThread;
    private Thread _messageDispatcherThread;

    private PlayerManager _manager;


    ISMessagesRpc _rpcProxy;
    private string _remoteUri = "";

    public ThalamusConnector(PlayerManager m)
    {
        _manager = m;
        _remoteUri = String.Format("http://{0}:{1}/", _remoteAddress, _remotePort);
        Debug.Log("ThalamusSueca endpoint set to " + _remoteUri);
        _rpcProxy = XmlRpcProxyGen.Create<ISMessagesRpc>();
        _rpcProxy.Timeout = 1000;
        _rpcProxy.Url = _remoteUri;


        _dispatcherThread = new Thread(DispatcherThreadThalamus);
        _messageDispatcherThread = new Thread(MessageDispatcherThalamus);
        _dispatcherThread.Start();
        _messageDispatcherThread.Start();
    }

    #region rpc stuff

    public void Dispose()
    {
        _shutdown = true;

        try
        {
            if (_listener != null) _listener.Stop();
        }
        catch { }

        try
        {
            if (_dispatcherThread != null) _dispatcherThread.Join();
        }
        catch { }

        try
        {
            if (_messageDispatcherThread != null) _messageDispatcherThread.Join();
        }
        catch { }
    }

    public void DispatcherThreadThalamus()
    {
        while (!_serviceRunning)
        {
            try
            {
                Debug.Log("Attempt to start service on port '" + _localPort + "'");
                _listener = new HttpListener();
                _listener.Prefixes.Add(string.Format("http://*:{0}/", _localPort));
                _listener.Start();
                Debug.Log("XMLRPC Listening on " + string.Format("http://*:{0}/", _localPort));
                _serviceRunning = true;
            }
            catch (Exception e)
            {
                _localPort++;
                Debug.Log(e.Message);
                Debug.Log("Port unavaliable.");
                _serviceRunning = false;
            }
        }

        while (!_shutdown)
        {
            try
            {
                HttpListenerContext context = _listener.GetContext();
                lock (_httpRequestsQueue)
                {
                    _httpRequestsQueue.Add(context);
                }
            }
            catch (Exception e)
            {
                if (_printExceptions) Debug.Log("Exception: " + e);
                _serviceRunning = false;
                if (_listener != null)
                    _listener.Close();
            }
        }
        Debug.Log("Terminated DispatcherThreadThalamus");
        //_listener.Close();
    }

    public void MessageDispatcherThalamus()
    {
        while (!_shutdown)
        {
            bool performSleep = true;
            try
            {
                if (_httpRequestsQueue.Count > 0)
                {
                    performSleep = false;
                    List<HttpListenerContext> httpRequests;
                    lock (_httpRequestsQueue)
                    {
                        httpRequests = new List<HttpListenerContext>(_httpRequestsQueue);
                        _httpRequestsQueue.Clear();
                    }
                    foreach (HttpListenerContext r in httpRequests)
                    {
                        //ProcessRequest(r);
                        (new Thread(ProcessRequestThalamus)).Start(r);
                        performSleep = false;
                    }
                }
            }
            catch (Exception e)
            {
                if (_printExceptions) Debug.Log("Exception: " + e);
            }
            if (performSleep) Thread.Sleep(10);
        }
        Debug.Log("Terminated MessageDispatcherThalamus");
    }

    public void ProcessRequestThalamus(object oContext)
    {
        try
        {
            XmlRpcListenerService svc = new ThalamusListener(_manager);
            svc.ProcessRequest((HttpListenerContext)oContext);
        }
        catch (Exception e)
        {
            if (_printExceptions) Debug.Log("Exception: " + e);
        }

    }

    #endregion


    public void Play(int player, string card, string playInfo, int floorId)
    {
        try
        {
            _rpcProxy.Play(player, card, playInfo, floorId);
        }
        catch (Exception e)
        {
            if (_printExceptions) Debug.Log("Exception: " + e.Message + (e.InnerException != null ? ": " + e.InnerException : ""));
        }
    }

    public void GameStart(int gameId, int playerId, int teamId, string trumpCard, int trumpCardPlayer, string[] cards, string partnerName, int floorId)
    {
        try
        {
            _rpcProxy.GameStart(gameId, playerId, teamId, trumpCard, trumpCardPlayer, cards, partnerName, floorId);
        }
        catch (Exception e)
        {
            if (_printExceptions) Debug.Log("Exception: " + e.Message + (e.InnerException != null ? ": " + e.InnerException : ""));
        }
    }

    public void NextPlayer(int playerId, int floorId)
    {
        try
        {
            _rpcProxy.NextPlayer(playerId, floorId);
        }
        catch (Exception e)
        {
            if (_printExceptions) Debug.Log("Exception: " + e.Message + (e.InnerException != null ? ": " + e.InnerException : ""));
        }
    }

    public void SessionStart(int sessionId, int numGames, int[] agentsIds, int floorId) 
    {
        try
        {
            _rpcProxy.SessionStart(sessionId, numGames, agentsIds, floorId);
        }
        catch (Exception e)
        {
            if (_printExceptions) Debug.Log("Exception: " + e.Message + (e.InnerException != null ? ": " + e.InnerException : ""));
        }
    }

    public void SessionEnd(int sessionId, int team0Score, int team1Score, int floorId)
    {
        try
        {
            _rpcProxy.SessionEnd(sessionId, team0Score, team1Score, floorId);
        }
        catch (Exception e)
        {
            if (_printExceptions) Debug.Log("Exception: " + e.Message + (e.InnerException != null ? ": " + e.InnerException : ""));
        }
    }

    public void Shuffle(int playerId, int floorId)
    {
        try
        {
            _rpcProxy.Shuffle(playerId, floorId);
        }
        catch (Exception e)
        {
            if (_printExceptions) Debug.Log("Exception: " + e.Message + (e.InnerException != null ? ": " + e.InnerException : ""));
        }
    }

    public void Deal(int playerId, int floorId)
    {
        try
        {
            _rpcProxy.Deal(playerId, floorId);
        }
        catch (Exception e)
        {
            if (_printExceptions) Debug.Log("Exception: " + e.Message + (e.InnerException != null ? ": " + e.InnerException : ""));
        }
    }

    public void Cut(int playerId, int floorId)
    {
        try
        {
            _rpcProxy.Cut(playerId, floorId);
        }
        catch (Exception e)
        {
            if (_printExceptions) Debug.Log("Exception: " + e.Message + (e.InnerException != null ? ": " + e.InnerException : ""));
        }
    }

    public void TrumpCard(string trumpCard, int playerId, int floorId)
    {
        try
        {
            _rpcProxy.TrumpCard(trumpCard, playerId, floorId);
        }
        catch (Exception e)
        {
            if (_printExceptions) Debug.Log("Exception: " + e.Message + (e.InnerException != null ? ": " + e.InnerException : ""));
        }
    }

    public void ReceiveRobotCards(int playerId, int floorId)
    {
        try
        {
            _rpcProxy.ReceiveRobotCards(playerId, floorId);
        }
        catch (Exception e)
        {
            if (_printExceptions) Debug.Log("Exception: " + e.Message + (e.InnerException != null ? ": " + e.InnerException : ""));
        }
    }

    public void TrickEnd(int winnerId, int trickPoints, int floorId)
    {
        try
        {
            _rpcProxy.TrickEnd(winnerId, trickPoints, floorId);
        }
        catch (Exception e)
        {
            if (_printExceptions) Debug.Log("Exception: " + e.Message + (e.InnerException != null ? ": " + e.InnerException : ""));
        }
    }

    public void Renounce(int playerId, int floorId)
    {
        try
        {
            _rpcProxy.Renounce(playerId, floorId);
        }
        catch (Exception e)
        {
            if (_printExceptions) Debug.Log("Exception: " + e.Message + (e.InnerException != null ? ": " + e.InnerException : ""));
        }
    }

    public void GameEnd(int team1Score, int team2Score, int floorId)
    {
        try
        {
            _rpcProxy.GameEnd(team1Score, team2Score, floorId);
        }
        catch (Exception e)
        {
            if (_printExceptions) Debug.Log("Exception: " + e.Message + (e.InnerException != null ? ": " + e.InnerException : ""));
        }
    }

    public void GazeAtScreen(double x, double y)
    {
        try
        {
            _rpcProxy.GazeAtScreen(x, y);
        }
        catch (Exception e)
        {
            if (_printExceptions) Debug.Log("Exception: " + e.Message + (e.InnerException != null ? ": " + e.InnerException : ""));
        }
    }

    public void TargetScreenInfo(string targetName, int x, int y)
    {
        try
        {
            _rpcProxy.TargetScreenInfo(targetName, x, y);
        }
        catch (Exception e)
        {
            if (_printExceptions) Debug.Log("Exception: " + e.Message + (e.InnerException != null ? ": " + e.InnerException : ""));
        }
    }

    public void GlanceAtScreen(double x, double y)
    {
        try
        {
            _rpcProxy.GlanceAtScreen(x, y);
        }
        catch (Exception e)
        {
            if (_printExceptions) Debug.Log("Exception: " + e.Message + (e.InnerException != null ? ": " + e.InnerException : ""));
        }

    }



}
