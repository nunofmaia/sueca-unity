﻿using UnityEngine;

public class HasCards : Flow.Predicate
{
    public override void Validate()
    {
        GameObject go = GameObject.FindGameObjectWithTag("GameController");
        Shuffle.ITrickCardGame cg;
        if (go == null)
        {
            Debug.Log("Oops, I don't think you have what it takes...");
            return;
        }
        else
        {
            var gm = go.GetComponent(typeof(GameManager)) as GameManager;
            if (gm == null)
            {
                Debug.Log("Oops, I don't think you have what it takes...");
                return;
            }

            cg = gm.State;
        }

        condition = cg.Tricks.Count + 1 < cg.NumberOfTricks;

        base.Validate();

    }
}
