﻿using System.Linq;
using UnityEngine;

public class IsPlayerCheating : Flow.Predicate
{
    public override void Validate()
    {
        GameObject go = GameObject.FindGameObjectWithTag("GameController");
        Sueca cg;
        if (go == null)
        {
            Debug.Log("Oops, I don't think you have what it takes...");
            return;
        }
        else
        {
            var gm = go.GetComponent(typeof(GameManager)) as GameManager;
            if (gm == null)
            {
                Debug.Log("Oops, I don't think you have what it takes...");
                return;
            }

            cg = gm.State;
        }

        var history = cg.History;
        var entry = history.Keys.ToList().Find(e =>
        {
            var card = (Shuffle.PlayingCard)cg.ActivePlay.Content;
            return e.Suit == card.Suit && e.PlayerId == cg.ActivePlay.PlayerId;
        });

        condition = history.ContainsKey(entry) && history[entry];
        if (condition)
        {
            cg.HasRenounced = true;
        }

        base.Validate();
    }
}
