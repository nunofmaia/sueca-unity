﻿using UnityEngine;

public class NextPlayer : Flow.Action
{
    public override void Validate()
    {
        GameObject go = GameObject.FindGameObjectWithTag("GameController");
        Shuffle.ITrickCardGame cg;
        if (go == null)
        {
            Debug.Log("Oops, I don't think you have what it takes...");
            return;
        }
        else
        {
            var gm = go.GetComponent(typeof(GameManager)) as GameManager;
            if (gm == null)
            {
                Debug.Log("Oops, I don't think you have what it takes...");
                return;
            }

            cg = gm.State;
        }

        cg.CurrentTrick.Plays.Add(cg.ActivePlay);

        var currentPlayerIndex = cg.Players.IndexOf(cg.ActivePlayer);
        var nextPlayerIndex = (currentPlayerIndex + 1) % cg.Players.Count;

        cg.ActivePlayer = cg.Players[nextPlayerIndex];

        //  GameManager.NextPlayer(nextPlayerIndex);

        base.Validate();
    }
}
