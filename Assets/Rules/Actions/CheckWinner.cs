﻿using UnityEngine;

public class CheckWinner : Flow.Action
{
    public override void Validate()
    {
        GameObject go = GameObject.FindGameObjectWithTag("GameController");
        Shuffle.ITrickCardGame cg;
        if (go == null)
        {
            Debug.Log("Oops, I don't think you have what it takes...");
            return;
        }
        else
        {
            var gm = go.GetComponent(typeof(GameManager)) as GameManager;
            if (gm == null)
            {
                Debug.Log("Oops, I don't think you have what it takes...");
                return;
            }

            cg = gm.State;
        }

        cg.CurrentTrick.Plays.Add(cg.ActivePlay);
        
        var winningPlay = cg.CurrentTrick.Plays[0];
        var winningCard = (Shuffle.PlayingCard)winningPlay.Content;
        Shuffle.Suit winningSuit = winningCard.Suit;
        
        for (int i = 1; i < cg.CurrentTrick.Plays.Count; i++)
        {
            var play = cg.CurrentTrick.Plays[i];
            var card = (Shuffle.PlayingCard)play.Content;
            
            if (cg.Trumps != null)
            {
                if (card.Suit == cg.Trumps && winningSuit != cg.Trumps)
                {
                    winningPlay = play;
                    winningSuit = card.Suit;
                }
                else if (card.Suit == winningSuit)
                {
                    winningPlay = CheckWinningPlay(winningPlay, play, cg.Trumps);
                }
            }
            else
            {
                winningPlay = CheckWinningPlay(winningPlay, play, cg.Trumps);   
            }
        }
        
        cg.TrickWinner = cg.Players.Find(player => player.Id == winningPlay.PlayerId);
        
        base.Validate();
    }

    Shuffle.Play CheckWinningPlay(Shuffle.Play currentWinningPlay, Shuffle.Play play, Shuffle.Suit? trumps)
    {
        var card1 = (Shuffle.PlayingCard)currentWinningPlay.Content;
        var card2 = (Shuffle.PlayingCard)play.Content;

        if (trumps != null && card1.Suit == trumps && card2.Suit != trumps)
        {
            return currentWinningPlay;
        }
        else if (trumps != null && card2.Suit == trumps && card1.Suit != trumps)
        {
            return play;
        }
        else if ((int)card1.Rank > (int)card2.Rank)
        {
            return currentWinningPlay;
        }
        else
        {
            return play;
        }
    }
}
