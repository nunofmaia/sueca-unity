﻿using System.Linq;
using UnityEngine;

public class MissingSuit : Flow.Action
{
    public override void Validate()
    {
        GameObject go = GameObject.FindGameObjectWithTag("GameController");
        Sueca cg;
        if (go == null)
        {
            Debug.Log("Oops, I don't think you have what it takes...");
            return;
        }
        else
        {
            var gm = go.GetComponent(typeof(GameManager)) as GameManager;
            if (gm == null)
            {
                Debug.Log("Oops, I don't think you have what it takes...");
                return;
            }

            cg = gm.State;
        }

        var history = cg.History;
        var entry = history.Keys.ToList().Find(e => e.Suit == cg.LeadSuit && e.PlayerId == cg.ActivePlay.PlayerId);

        Debug.Log(string.Format("Miss {0}, {1}", entry.PlayerId, entry.Suit));
        history[entry] = true;

        base.Validate();
    }
}
