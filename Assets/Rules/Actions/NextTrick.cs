﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class NextTrick : Flow.Action
{
    public override void Validate()
    {
        GameObject go = GameObject.FindGameObjectWithTag("GameController");
        Shuffle.ITrickCardGame cg;
        if (go == null)
        {
            Debug.Log("Oops, I don't think you have what it takes...");
            return;
        }
        else
        {
            var gm = go.GetComponent(typeof(GameManager)) as GameManager;
            if (gm == null)
            {
                Debug.Log("Oops, I don't think you have what it takes...");
                return;
            }

            cg = gm.State;
        }

        var winner = (Shuffle.ITeamPlayer)cg.TrickWinner;
        var plays = new List<Shuffle.Play>();

        cg.CurrentTrick.Plays.ForEach(play => plays.Add(play));

        var winningTrick = new Shuffle.Trick(plays, winner);

        cg.ActivePlayer = winner;
        cg.Tricks.Add(winningTrick);

        //GameManager.NextPlayer(cg.TrickWinner.Id);

        cg.TrickWinner = null;
        cg.LeadSuit = null;
        //cg.CurrentTrick.Plays.Clear();

        base.Validate();

        //EventManager.Trigger(Events.NEXT_TRICK, EventArgs.Empty);
    }
}
