﻿using System;
using UnityEngine;

public class CalculateScore : Flow.Action
{
    public override void Validate()
    {
        GameObject go = GameObject.FindGameObjectWithTag("GameController");
        Shuffle.ITrickCardGame cg;
        if (go == null)
        {
            Debug.Log("Oops, I don't think you have what it takes...");
            return;
        }
        else
        {
            var gm = go.GetComponent(typeof (GameManager)) as GameManager;
            if (gm == null)
            {
                Debug.Log("Oops, I don't think you have what it takes...");
                return;
            }

            cg = gm.State;
        }

        int totalScore = 0;

        foreach (var p in cg.CurrentTrick.Plays)
        {
            var card = (Shuffle.PlayingCard)p.Content;
            totalScore += card.Value;
        }

        cg.TrickWinner.Score += totalScore;

        base.Validate();
    }
}
